/** @jsx jsx */
import React, {useState, useEffect, useRef} from 'react'
import {jsx} from '@emotion/core'
import { InputGroup, Input, Button, useToast, Layer, IconUserPlus, Select, IconX, IconPlus, CloseButton, Spinner, IconCheck } from 'sancho'
import TEXT from '../constants/text-es'
import Axios from 'axios'
import API_ROUTES from '../constants/api-routes'
import HTTP_STATUS from '../constants/http-status'
import ALERTS from '../constants/alerts'
import USER_TYPES from '../constants/user-types'
import GeneralHelpers from '../helpers/general-helpers'
import { isMobile } from 'react-device-detect';

const SignUp = (props) => {

  let today = new Date()
  today.setFullYear(today.getFullYear() - 11)
  var maxDateStr = GeneralHelpers.toISOString(today)

  const toast = useToast()

  const layerStyle = {
    margin: '1.5rem',
    padding: '1.5rem',
    fontFamily: 'Tiresias',
  },
  mainDivStyle = {
    padding: '1.5rem',
    fontFamily: 'Tiresias',
  },
  divStyle = {
    paddingRight: '15px',
    overflowY: 'scroll',
    maxHeight: '60vh'
  },
  buttonStyle = {
    marginTop: '1rem'
  },
  firstPhoneInputGroupStyle = {
    marginTop: '0'
  },
  phoneDivStyle = {
    display: 'flex'
  },
  availabilityHelperStyle = {
    padding: '0.5rem',
    fontSize: '0.8em',
    display: 'flex',
    alignItems: 'center'
  },
  availabilityHelperIconStyle = {
    marginRight: '0.25rem'
  }

  const [userName, setUserName] = useState(''),
        [email, setEmail] = useState(''),
        [doc, setDoc] = useState(''),
        [firstName, setFirstName] = useState(''),
        [lastName, setLastName] = useState(''),
        [birthday, setBirthday] = useState(maxDateStr),
        [birthdayWasSet, setBirthdayWasSet] = useState(false),
        [userType, setUserType] = useState(USER_TYPES.STUDENT),
        [subjectList, setSubjectList] = useState([]),
        [selectedSubject, setSelectedSubject] = useState(0),
        [subjects, setSubjects] = useState([]),
        [phones, setPhones] = useState(['']),
        [password, setPassword] = useState(''),
        [passwordConfirmation, setPasswordConfirmation] = useState(''),
        [userNameError, setUserNameError] = useState(null),
        [emailError, setEmailError] = useState(null),
        [docError, setDocError] = useState(null),
        [firstNameError, setFirstNameError] = useState(null),
        [lastNameError, setLastNameError] = useState(null),
        [birthdayError, setBirthdayError] = useState(null),
        [passwordError, setPasswordError] = useState(null),
        [passwordConfirmationError, setPasswordConfirmationError] = useState(null),
        [phonesError, setPhonesError] = useState(null),
        [selectedSubjectError, setSelectedSubjectError] = useState(null),
        [registering, setRegistering] = useState(false),
        [userNameAvailability, setUserNameAvailability] = useState(null),
        [emailAvailability, setEmailAvailability] = useState(null),
        [docAvailability, setDocAvailability] = useState(null),
        mainDivRef = useRef(null)

  useEffect(() => {
    mainDivRef.current.scrollTop = 0
    Axios.get(API_ROUTES.SUBJECTS)
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setSubjectList(response.data)
      else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(!response)
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleUserTypeChangee = (ev) => {
    setUserType(Number(ev.currentTarget.value))
  }

  const checkCredentialAvailability = (credential, setter) => {
    Axios.get(API_ROUTES.USERS.EXISTS, {params: {id: credential}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setter(true)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.CONFLICT)
          setter(false)
      }
    })
  }

  useEffect(() => {
    if(GeneralHelpers.isUserName(userName)) {
      setUserNameAvailability(undefined)
      checkCredentialAvailability('@' + userName, setUserNameAvailability)
    }
  }, [userName])

  const handleUserNameChange = (ev) => {
    let newUserName = ev.target.value
    setUserNameAvailability(null)
    setUserName(newUserName)
    setUserNameError(null)
  }

  useEffect(() => {
    if(GeneralHelpers.isEmail(email)) {
      setEmailAvailability(undefined)
      checkCredentialAvailability(email, setEmailAvailability)
    }
  }, [email])

  const handleEmailChange = (ev) => {
    setEmail(ev.target.value)
    setEmailAvailability(null)
    setEmailError(null)
  }

  useEffect(() => {
    if(GeneralHelpers.isCedula(doc)) {
      setDocAvailability(undefined)
      checkCredentialAvailability(doc, setDocAvailability)
    }
  }, [doc])

  const handleDocumentChange = (ev) => {
    setDoc(ev.target.value)
    setDocAvailability(null)
    setDocError(null)
  }

  const handleFirstNameChange = (ev) => {
    setFirstName(ev.target.value)
    setFirstNameError(null)
  }

  const handleLastNameChange = (ev) => {
    setLastName(ev.target.value)
    setLastNameError(null)
  }

  const handleBirthdayChange = (ev) => {
    let todayStr = GeneralHelpers.toISOString(new Date())
    maxDateStr = todayStr
    setBirthday(ev.target.value)
    setBirthdayError(null)
    setBirthdayWasSet(true)
  }

  const handlePasswordChange = (ev) => {
    setPassword(ev.target.value)
    setPasswordError(null)
  }

  const handlePasswordConfirmationChange = (ev) => {
    setPasswordConfirmation(ev.target.value)
    setPasswordConfirmationError(null)
  }

  const handleSubjectChange = (ev) => {
    let subjectId = Number(ev.target.value),
        newSubjects = subjects
    if(subjectId > -1 && !subjects.includes(subjectId)) {
      setSelectedSubject(subjectId)
      newSubjects.push(subjectId)
      setSubjects(newSubjects)
      setSelectedSubjectError(null)
    }
  }

  const handleSubjectDelete = (value) => {
    let newSubjects = [...subjects]
    newSubjects.splice(value, 1)
    setSubjects(newSubjects)
  }

  const handlePhoneChange = (phone, index) => {
    let newPhones = [...phones]
    newPhones[index] = phone
    setPhones(newPhones)
    setPhonesError(null)
  }

  const handlePhoneDelete = (index) => {
    let newPhones = [...phones]
    newPhones.splice(index, 1)
    setPhones(newPhones)
  }

  const isDataValid = () => {

    if(!userType)
      return false

    var validFieldsNumber = 0
    const numberOfFields = userType === USER_TYPES.STUDENT ? 9 : 10

    if(userName) {
      if(GeneralHelpers.isUserName(userName))
        validFieldsNumber++
      else
        setUserNameError(TEXT.SIGNUP.FIELDS.USERNAME.ERRORS.FORMAT)
    } else
      setUserNameError(TEXT.SIGNUP.FIELDS.USERNAME.ERRORS.PRESENCE)

    if(email) {
      if(GeneralHelpers.isEmail(email))
        validFieldsNumber++
      else
        setEmailError(TEXT.SIGNUP.FIELDS.EMAIL.ERRORS.FORMAT)
    } else
      setEmailError(TEXT.SIGNUP.FIELDS.EMAIL.ERRORS.PRESENCE)
      
    if(doc) {
      if(GeneralHelpers.isCedula(doc))
        validFieldsNumber++
      else
        setDocError(TEXT.SIGNUP.FIELDS.DOCUMENT.ERRORS.FORMAT)
    } else
      setDocError(TEXT.SIGNUP.FIELDS.DOCUMENT.ERRORS.PRESENCE)

    if(firstName) {
      if(firstName < 256)
        validFieldsNumber++
      else
        setFirstNameError(TEXT.SIGNUP.FIELDS.FIRST_NAME.ERRORS.FORMAT)
    } else
      setFirstNameError(TEXT.SIGNUP.FIELDS.FIRST_NAME.ERRORS.PRESENCE)

    if(lastName)
      if(lastName < 256)
        validFieldsNumber++
      else
        setLastNameError(TEXT.SIGNUP.FIELDS.LAST_NAME.ERRORS.FORMAT)
    else
      setLastNameError(TEXT.SIGNUP.FIELDS.LAST_NAME.ERRORS.PRESENCE)

    if(birthday && birthdayWasSet)
      validFieldsNumber++
    else
      setBirthdayError(TEXT.SIGNUP.FIELDS.BIRTHDAY.ERRORS.PRESENCE)

    if(password) {
      if(password.length > 7)
        validFieldsNumber++
      else
        setPasswordError(TEXT.SIGNUP.FIELDS.PASSWORD.ERRORS.FORMAT)
    } else
      setPasswordError(TEXT.SIGNUP.FIELDS.PASSWORD.ERRORS.PRESENCE)

    if(passwordConfirmation) {
      if(passwordConfirmation === password)
        validFieldsNumber++
      else
        setPasswordConfirmationError(TEXT.SIGNUP.FIELDS.PASSWORD_CONFIRMATION.ERRORS.FORMAT)
    } else
      setPasswordConfirmationError(TEXT.SIGNUP.FIELDS.PASSWORD_CONFIRMATION.ERRORS.PRESENCE)

    if(phones && phones.length > 0) {
      if(!phones.some((phone) => (!GeneralHelpers.isPhone(phone))))
        validFieldsNumber++
      else
        setPhonesError(TEXT.SIGNUP.FIELDS.PHONES.ERRORS.FORMAT)
    }
    else
      setPhonesError(TEXT.SIGNUP.FIELDS.PHONES.ERRORS.PRESENCE)

    if(userType === USER_TYPES.TEACHER) {
      if(subjects && subjects.length > 0)
        validFieldsNumber++
      else
        setSelectedSubjectError(TEXT.SIGNUP.FIELDS.SUBJECTS.ERRORS.PRESENCE)
    }

    return validFieldsNumber === numberOfFields
  }

  const handleSignUpButtonClick = () => {

    if(isDataValid()) {
      setRegistering(true)
      var dataToSend = {
        username: userName,
        email,
        document: doc,
        name: firstName,
        surname: lastName,
        birthday,
        phones,
        password
      },
      route = null
      if(userType === USER_TYPES.TEACHER) {
        dataToSend.subjects = subjects
        route = API_ROUTES.USERS.TEACHERS
      } else
        route = API_ROUTES.USERS.STUDENTS
      Axios.post(route, dataToSend)
      .then((response) => {
        setRegistering(false)
        if(response.status === HTTP_STATUS.OK) {
          resetForm()
          props.onReturnLogin()
          toast(ALERTS.SIGNUP.SUCCESS)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
      .catch((err) => {
        setRegistering(false)
        var response = err.response
        if(response.status === HTTP_STATUS.CONFLICT)
          toast(ALERTS.SIGNUP.USER_ALREADY_EXISTS)
        else if(response.status === HTTP_STATUS.BAD_REQUEST)
          toast(ALERTS.SIGNUP.SIGNUP_FORM_ERROR)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
    }

  }

  const resetForm = () => {
    setUserName('')
    setEmail('')
    setDoc('')
    setFirstName('')
    setLastName('')
    setBirthday(maxDateStr)
    setBirthdayWasSet(false)
    setUserType(USER_TYPES.STUDENT)
    setSelectedSubject(0)
    setSubjects([])
    setPhones([''])
    setPassword('')
    setPasswordConfirmation('')
    resetErrors()
  }

  const resetErrors = () => {
    setUserNameError(null)
    setDocError(null)
    setEmailError(null)
    setFirstNameError(null)
    setLastNameError(null)
    setBirthdayError(null)
    setPhonesError(null)
    setSelectedSubjectError(null)
    setPasswordError(null)
    setPasswordConfirmationError(null)
  }

  const handleLogInButtonClick = () => {
    resetErrors()
    props.onReturnLogin()
  }

  const getSubjectButtons = () => {
    return subjects.map((subjectId, index) => {
      let subject = subjectList.find((subject) => (subject.id === subjectId))
      return(
        <Button variant='outline' key={index} block css={buttonStyle} iconAfter={<IconX/>} onClick={() => handleSubjectDelete(index)}>
          {subject.name}
        </Button>
      )
    })
  }

  const getPhoneRows = () => {
    return phones.map((phone, index) => {
      if(index === 0)
        return(
          <InputGroup key={index} label='tel' css={firstPhoneInputGroupStyle} hideLabel>
            <Input type='tel' placeholder={TEXT.SIGNUP.FIELDS.PHONES.PLACEHOLDER} value={phones[index]} onChange={(ev) => (handlePhoneChange(ev.target.value, index))}/>
          </InputGroup>
        )
      else
        return (
          <InputGroup key={index} label='tel' hideLabel>
            <div css={phoneDivStyle}>
              <Input type='tel' placeholder={TEXT.SIGNUP.FIELDS.PHONES.PLACEHOLDER} value={phones[index]} onChange={(ev) => (handlePhoneChange(ev.target.value, index))}/>
              <CloseButton onClick={() => handlePhoneDelete(index)}/>
            </div>
          </InputGroup>
        )
    })
  }

  const addPhoneField = () => {
    let newPhones = [...phones]
    newPhones.push('')
    setPhones(newPhones)
    setPhonesError(null)
  }

  const getSubjectOptions = () => {
    return subjectList.map((subject) => (
      <option key={subject.id} value={subject.id}>{subject.name}</option>
    ))
  }

  const getAvailabilityHelper = (credential, available) => {
    if(credential.length > 0) {
      if(available === true)
        return(
          <div css={availabilityHelperStyle}>
            <IconCheck color='green' css={availabilityHelperIconStyle}/>
            {TEXT.SIGNUP.AVAILABILITY_HELPER.SUCCESS}
          </div>
        )
      else if(available === undefined)
        return(
          <div css={availabilityHelperStyle}>
            <Spinner size='sm' delay={0} css={availabilityHelperIconStyle}/>
            {TEXT.SIGNUP.AVAILABILITY_HELPER.CHECKING}
          </div>
        )
      else if(available === false)
        return(
          <div css={availabilityHelperStyle}>
            <IconX color='red' css={availabilityHelperIconStyle}/>
            {TEXT.SIGNUP.AVAILABILITY_HELPER.FAIL}
          </div>
        )
      else if(available === null)
        return null
    }
  }

  const render = () => (
    <div css={divStyle} ref={mainDivRef}>
      <InputGroup label={TEXT.SIGNUP.FIELDS.USER_TYPE.LABEL}>
        <Select value={userType} onChange={handleUserTypeChangee}>
          <option value={USER_TYPES.STUDENT}>{TEXT.SIGNUP.FIELDS.USER_TYPE.OPTIONS.STUDENT}</option>
          <option value={USER_TYPES.TEACHER}>{TEXT.SIGNUP.FIELDS.USER_TYPE.OPTIONS.TEACHER}</option>
        </Select>
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.USERNAME.LABEL} error={userNameError}>
        <Input placeholder={TEXT.SIGNUP.FIELDS.USERNAME.PLACEHOLDER} value={userName} onChange={handleUserNameChange}/>
        {getAvailabilityHelper(userName, userNameAvailability)}
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.EMAIL.LABEL} error={emailError}>
        <Input type='email' placeholder={TEXT.SIGNUP.FIELDS.EMAIL.PLACEHOLDER} value={email} onChange={handleEmailChange}/>
        {getAvailabilityHelper(email, emailAvailability)}
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.DOCUMENT.LABEL} error={docError}>
        <Input type='text' placeholder={TEXT.SIGNUP.FIELDS.DOCUMENT.PLACEHOLDER} value={doc} onChange={handleDocumentChange}/>
        {getAvailabilityHelper(doc, docAvailability)}
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.FIRST_NAME.LABEL} error={firstNameError}>
        <Input type='text' placeholder={TEXT.SIGNUP.FIELDS.FIRST_NAME.PLACEHOLDER} value={firstName} onChange={handleFirstNameChange}/>
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.LAST_NAME.LABEL} error={lastNameError}>
        <Input type='text' placeholder={TEXT.SIGNUP.FIELDS.LAST_NAME.PLACEHOLDER} value={lastName} onChange={handleLastNameChange}/>
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.BIRTHDAY.LABEL} error={birthdayError}>
        <Input type='date' placeholder={TEXT.SIGNUP.FIELDS.BIRTHDAY.PLACEHOLDER} value={birthday} max={maxDateStr} onChange={handleBirthdayChange}/>
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.PHONES.LABEL} error={phonesError}>
        {getPhoneRows()}
      </InputGroup>
      <Button variant='outline' block css={buttonStyle} iconBefore={<IconPlus/>} onClick={addPhoneField}>
        {TEXT.SIGNUP.FIELDS.PHONES.ADD_BUTTON}
      </Button>
      <InputGroup label={TEXT.SIGNUP.FIELDS.PASSWORD.LABEL} error={passwordError}>
        <Input type='password' placeholder={TEXT.SIGNUP.FIELDS.PASSWORD.PLACEHOLDER} value={password} onChange={handlePasswordChange}/>
      </InputGroup>
      <InputGroup label={TEXT.SIGNUP.FIELDS.PASSWORD_CONFIRMATION.LABEL} error={passwordConfirmationError}>
        <Input type='password' placeholder={TEXT.SIGNUP.FIELDS.PASSWORD_CONFIRMATION.PLACEHOLDER} value={passwordConfirmation} onChange={handlePasswordConfirmationChange}/>
      </InputGroup>

      {
        userType === USER_TYPES.TEACHER &&
        <React.Fragment>
          <InputGroup label={TEXT.SIGNUP.FIELDS.SUBJECTS.LABEL} error={selectedSubjectError}>
            <Select value={selectedSubject} onChange={handleSubjectChange}>
              <option value={-1}>{TEXT.SIGNUP.FIELDS.SUBJECTS.LABEL}</option>
              {getSubjectOptions()}
            </Select>
          </InputGroup>
          {getSubjectButtons()}
        </React.Fragment>
      }

      <Button variant='outline' block css={buttonStyle} intent='primary' iconBefore={<IconUserPlus/>} loading={registering} onClick={handleSignUpButtonClick}>
        {TEXT.SIGNUP.SIGNUP_BUTTON}
      </Button>
      <Button variant='ghost' block css={buttonStyle} onClick={handleLogInButtonClick}>
        {TEXT.SIGNUP.LOGIN_ANCHOR}
      </Button>
    </div>
  )

  if(isMobile)
    return(
      <div css={mainDivStyle}>
        {render()}
      </div>
    )
  else
    return(
      <Layer css={layerStyle}>
        {render()}
      </Layer>
    )
}

export default SignUp