/** @jsx jsx */
import { jsx } from '@emotion/core'
import {withRouter} from 'react-router-dom'
import { Button, IconLogOut, useToast } from 'sancho'
import AuthHelpers from '../../helpers/auth-helpers';
import TEXT from '../../constants/text-es';

const ProfileTab = (props) => {

  const toast = useToast()

  return(
    <Button
      variant='outline'
      intent='primary'
      block
      iconBefore={<IconLogOut/>}
      onClick={() => AuthHelpers.discardTokenAndRedirect(props, toast, true)}
    >
      {TEXT.HOME.TABS.GENERAL.PROFILE.LOGOUT}
    </Button>
  )
}

export default withRouter(ProfileTab)