/** @jsx jsx */
import { useState, useEffect } from 'react'
import { jsx, css } from '@emotion/core'
import {withRouter} from 'react-router-dom'
import { useToast, TabPanel, Table, TableHead, TableRow, TableCell, TableBody, Button, IconPlus, IconButton, IconMoreVertical, Dialog } from 'sancho'
import TEXT from '../../../constants/text-es';
import CurriculaWidget from '../../../common/curricula-widget';
import Axios from 'axios';
import API_ROUTES from '../../../constants/api-routes';
import AuthHelpers from '../../../helpers/auth-helpers';
import HTTP_STATUS from '../../../constants/http-status';
import ALERTS from '../../../constants/alerts';

const CalendarTab = (props) => {

  const [curriculas, setCurriculas] = useState([]),
        [modalsOpen, setModalsOpen] = useState([]),
        [teachers, setTeachers] = useState([]),
        [classrooms, setClassrooms] = useState([]),
        [freeClassrooms, setFreeClassrooms] = useState([]),
        [subjects, setSubjects] = useState([]),
        [addingCurricula, setAddingCurricula] = useState(false)

  const toast = useToast()

  const tabStyle = css`
    padding: 1rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  `,
  dialogMainDivStyle = css`
    padding: 1rem;
  `

  const getSubjects = () => {
    Axios.get(API_ROUTES.SUBJECTS, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        let newSubjects = response.data === 404 ? [] : response.data
        setSubjects(newSubjects)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const getTeachers = () => {
    Axios.get(API_ROUTES.ADMINS.TEACHERS, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setTeachers(response.data)
      else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(!response)
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const getClassrooms = () => {
    Axios.get(API_ROUTES.ADMINS.CLASSROOMS, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        let data = response.data === 404 ? {} : response.data,
            newClassrooms = data.classrooms || []
        setClassrooms(newClassrooms)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const getFreeClassrooms = (day, periodStart, periodEnd, subjectId) => {
    let params = {
      day,
      start: periodStart,
      end: periodEnd,
      subject: subjectId
    }
    Axios.get(API_ROUTES.ADMINS.FREE_CLASSROOMS, {params, headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setFreeClassrooms(response.data)
      else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(!response)
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  useEffect(() => {
    if(props.active) {
      getTeachers()
      getSubjects()
      getClassrooms()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.active])

  const addNewCurricula = () => {
    let newCurriculas = [...curriculas],
        newModalsOpen = [...modalsOpen]
    newCurriculas.push({

    })
    newModalsOpen.push(true)
    setAddingCurricula(true)
    setCurriculas(newCurriculas)
    setModalsOpen(newModalsOpen)
  }

  const createCurricula = (lessons, subjectId) => {
    var dataToSend = {
      id_subject: subjectId,
      week: lessons
    }
    Axios.post(API_ROUTES.ADMINS.LESSONS, dataToSend, {headers: {Authorization: AuthHelpers.getToken(props)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        toast(ALERTS.LESSONS.SUCCESS_CREATE)
      }
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const openModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = true
    setModalsOpen(newModalsOpen)
  }

  const closeModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = false
    setModalsOpen(newModalsOpen)
    setAddingCurricula(false)
  }

  const getCurriculaDialog = (curricula, index) => {
    return(
      <Dialog
        isOpen={modalsOpen[index]}
        mobileFullscreen
        onRequestClose={() => closeModal(index)}
        css={css`max-width: 720px;`}
      >
        <div css={dialogMainDivStyle}>
          <CurriculaWidget
            addingCurricula={addingCurricula}
            subjects={subjects}
            teachers={teachers}
            allClassrooms={classrooms}
            freeClassrooms={freeClassrooms}
            getFreeClassrooms={getFreeClassrooms}
            onClose={() => closeModal(index)}
            onCreate={createCurricula}
          />
        </div>
      </Dialog>
    )
  }

  const getCurriculaRows = () => {
    if(curriculas.length > 0 && modalsOpen.length === curriculas.length)
      return curriculas.map((curricula, index) => {
        return(
          <TableRow key={index}>
            <TableCell>{index}</TableCell>
            <TableCell align='right'>
              <IconButton variant='ghost' icon={<IconMoreVertical/>} label={TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.GHOST_BUTTON_LABEL} onClick={() => openModal(index)}/>
              {getCurriculaDialog(curricula, index)}
            </TableCell>
          </TableRow>
        )
      })
    else
      return(
        <TableRow>
          <TableCell colSpan={2}>{TEXT.HOME.VIEWS.ADMIN.CALENDAR.TABLE.NO_CURRICULAS_FOUND}</TableCell>
        </TableRow>
      )
  }

  if(props.active)
    return(
      <TabPanel id="admissions" className="Tab-panel" css={tabStyle}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.CALENDAR.TABLE.SUBJECT}</TableCell>
              <TableCell align='right'></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {getCurriculaRows()}
          </TableBody>
        </Table>
        <Button
          variant='outline'
          block
          intent='primary'
          iconBefore={<IconPlus/>}
          css={css`margin-top: 1rem;`}
          onClick={addNewCurricula}
        >
          {TEXT.HOME.VIEWS.ADMIN.CALENDAR.ADD_BUTTON}
        </Button>
      </TabPanel>
    )
  else
    return null
}

export default withRouter(CalendarTab)