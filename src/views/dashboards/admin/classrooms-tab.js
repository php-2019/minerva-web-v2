/** @jsx jsx */
import { useState, useEffect } from 'react'
import { jsx, css } from '@emotion/core'
import {withRouter} from 'react-router-dom'
import { useToast, TabPanel, Table, TableHead, TableRow, TableCell, TableBody, Button, IconButton, IconMoreVertical, Dialog, IconPlus, InputGroup, Input, IconCheck, IconX } from 'sancho'
import TEXT from '../../../constants/text-es'
import Axios from 'axios';
import API_ROUTES from '../../../constants/api-routes';
import HTTP_STATUS from '../../../constants/http-status';
import ALERTS from '../../../constants/alerts';
import AuthHelpers from '../../../helpers/auth-helpers';

const ClassroomsTab = (props) => {

  const [classrooms, setClassrooms] = useState([]),
        [modalsOpen, setModalsOpen] = useState([]),
        [floorBounds, setFloorBounds] = useState([0, 0]),
        [newClassroomName, setNewClassroomName] = useState(''),
        [newClassroomNameError, setNewClassroomNameError] = useState(''),
        [newClassroomCapacity, setNewClassroomCapacity] = useState(0),
        [newClassroomFloor, setNewClassroomFloor] = useState(0),
        [newClassroomCapacityError, setNewClassroomCapacityError] = useState(null),
        [addingClassroom, setAddingClassroom] = useState(false)

  const toast = useToast()
  
  const tabStyle = css`
    padding: 1rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  `,
  dialogMainDivStyle = css`
    padding: 1rem;
  `

  const getClassrooms = () => {
    Axios.get(API_ROUTES.ADMINS.CLASSROOMS, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        let data = response.data === 404 ? {} : response.data,
            newClassrooms = data.classrooms || [],
            newFloorBounds = data.bounds || [0, 0],
            newModalsOpen = new Array(newClassrooms.length).fill(false)
        setClassrooms(newClassrooms)
        setModalsOpen(newModalsOpen)
        setFloorBounds(newFloorBounds)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  useEffect(() => {
    if(props.active)
      getClassrooms()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.active])

  const addNewClassroom = () => {
    let newClassrooms = [...classrooms],
        newModalsOpen = [...modalsOpen]
    newClassrooms.push({
      name: TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.CREATING_CLASSROOM,
      capacity: TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.UNDEFINED_CAPACITY,
      floor: TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.UNDEFINED_FLOOR
    })
    newModalsOpen.push(true)
    setClassrooms(newClassrooms)
    setModalsOpen(newModalsOpen)
    setAddingClassroom(true)
  }
  
  const openModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = true
    setNewClassroomName(classrooms[index].name)
    setNewClassroomCapacity(classrooms[index].capacity)
    setModalsOpen(newModalsOpen)
  }

  const closeModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = false
    if(addingClassroom) {
      let newClassrooms = [...classrooms]
      newClassrooms.pop()
      newModalsOpen.pop()
      setClassrooms(newClassrooms)
      setAddingClassroom(false)
      setNewClassroomFloor(0)
    }
    setNewClassroomCapacity(0)
    setNewClassroomName('')
    setModalsOpen(newModalsOpen)
  }

  const handleNameChange = (ev) => {
    setNewClassroomName(ev.target.value)
    setNewClassroomNameError(null)
  }

  const handleCapacityChange = (ev) => {
    setNewClassroomCapacity(Number(ev.target.value))
    setNewClassroomCapacityError(null)
  }

  const handleFloorChange = (ev) => {
    setNewClassroomFloor(Number(ev.target.value))
  }

  const saveClassroomChanges = (index) => {
    const numberOfFields = 2
    var validFieldsNumber = 0,
        dataToSend = {
          id: classrooms[index].id
        }
    if(newClassroomName === classrooms[index].name)
      validFieldsNumber++
    else {
      if(!classrooms.some((classroom) => (classroom.name === newClassroomName))) {
        if(newClassroomName.length > 0) {
          if(newClassroomName.length < 256) {
            dataToSend.name = newClassroomName
            validFieldsNumber++
          } else
            setNewClassroomNameError(TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.DIALOG.NAME_FIELD.ERRORS.FORMAT)
        } else
          setNewClassroomNameError(TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.DIALOG.NAME_FIELD.ERRORS.PRESENCE)
      } else
        setNewClassroomNameError(TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.DIALOG.NAME_FIELD.ERRORS.UNIQUENESS)
    }

    if(newClassroomCapacity === classrooms[index].capacity)
      validFieldsNumber++
    else {
      if(newClassroomCapacity > 0) {
        dataToSend.capacity = newClassroomCapacity
        validFieldsNumber++
      } else
        setNewClassroomCapacityError(TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.DIALOG.CAPACITY_FIELD.ERRORS.FORMAT)
    }

    if(addingClassroom)
      dataToSend.floor = newClassroomFloor

    if(validFieldsNumber < numberOfFields)
      return

    if(dataToSend.name || dataToSend.capacity || dataToSend.floor) {
      let action = addingClassroom ? Axios.post : Axios.patch
      action(API_ROUTES.ADMINS.CLASSROOMS, dataToSend, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
      .then((response) => {
        if(response.status === HTTP_STATUS.OK) {
          let newClassrooms = [...classrooms],
              newModalsOpen = [...modalsOpen]
          if(addingClassroom) {
            let classroomId = response.data
            newClassrooms[index] = {
              id: classroomId,
              name: newClassroomName,
              capacity: newClassroomCapacity,
              floor: newClassroomFloor
            }
            setAddingClassroom(false)
          } else {
            if(dataToSend.name)
              newClassrooms[index].name = newClassroomName
            if(dataToSend.capacity)
              newClassrooms[index].capacity = newClassroomCapacity
          }
          newModalsOpen[index] = false
          setClassrooms(newClassrooms)
          setModalsOpen(newModalsOpen)
          setNewClassroomCapacity(0)
          setNewClassroomName('')
          setNewClassroomFloor(0)
          toast(addingClassroom ? ALERTS.CLASSROOMS.SUCCESS_CREATE : ALERTS.CLASSROOMS.SUCCESS_EDIT)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
      .catch((err) => {
        console.error(err)
        let response = err.response
        if(response) {
          if(response.status === HTTP_STATUS.UNAUTHORIZED)
            AuthHelpers.discardTokenAndRedirect(props, toast)
          else
            toast(ALERTS.GENERAL.OUR_ERROR)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
    }
  }

  const getClassroomRows = () => {
    if(classrooms.length > 0 && modalsOpen.length === classrooms.length)
      return classrooms.map((classroom, index) => {
        return(
          <TableRow key={index}>
            <TableCell>{classroom.name}</TableCell>
            <TableCell>{classroom.capacity}</TableCell>
            <TableCell>{classroom.floor}</TableCell>
            <TableCell align='right'>
              <IconButton variant='ghost' icon={<IconMoreVertical/>} label={TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.GHOST_BUTTON_LABEL} onClick={() => openModal(index)}/>
              <Dialog isOpen={modalsOpen[index]} mobileFullscreen onRequestClose={() => closeModal(index)}>
                <div css={dialogMainDivStyle}>
                  <div css={css`display: flex; justify-content: flex-end;`}>
                  <IconButton
                    label=''
                    variant='ghost'
                    intent='success'
                    icon={<IconCheck/>}
                    onClick={() => saveClassroomChanges(index)}
                  />
                  <IconButton
                    label=''
                    variant='ghost'
                    intent='danger'
                    icon={<IconX/>}
                    onClick={() => closeModal(index)}
                  />
                  </div>
                  <InputGroup
                    label={TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.DIALOG.NAME_FIELD.LABEL}
                    error={newClassroomNameError}
                  >
                    <Input value={newClassroomName} onChange={handleNameChange}/>
                  </InputGroup>
                  <InputGroup
                    label={TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.DIALOG.CAPACITY_FIELD.LABEL}
                    error={newClassroomCapacityError}
                  >
                    <Input type='number' value={newClassroomCapacity} onChange={handleCapacityChange}/>
                  </InputGroup>
                  {
                    addingClassroom &&
                    <InputGroup
                      label={TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.DIALOG.FLOOR_FIELD.LABEL}
                    >
                      <Input
                        type='number'
                        value={newClassroomFloor}
                        onChange={handleFloorChange}
                        min={floorBounds[0]}
                        max={floorBounds[1]}
                      />
                    </InputGroup>
                  }
                </div>
              </Dialog>
            </TableCell>
          </TableRow>
        )
      })
    else
      return(
        <TableRow>
          <TableCell colSpan={3}>{TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.TABLE.NO_CLASSROOMS_FOUND}</TableCell>
        </TableRow>
      )
  }

  if(props.active)
    return(
      <TabPanel id="admissions" className="Tab-panel" css={tabStyle}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.TABLE.HEADERS.NAME}</TableCell>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.TABLE.HEADERS.CAPACITY}</TableCell>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.TABLE.HEADERS.FLOOR}</TableCell>
              <TableCell align='right'></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {getClassroomRows()}
          </TableBody>
        </Table>
        <Button
          variant='outline'
          block
          intent='primary'
          iconBefore={<IconPlus/>}
          css={css`margin-top: 1rem;`}
          onClick={addNewClassroom}
        >
          {TEXT.HOME.VIEWS.ADMIN.CLASSROOMS.ADD_BUTTON}
        </Button>
      </TabPanel>
    )
  else
    return null

}

export default withRouter(ClassroomsTab)