/** @jsx jsx */
import React, { useState, useEffect } from 'react'
import { jsx, css } from '@emotion/core'
import Axios from 'axios'
import {withRouter} from 'react-router-dom'
import { useToast, TabPanel, Table, TableHead, TableRow, TableCell, TableBody, IconButton, IconMoreVertical, Button, IconPlus, MenuList, MenuItem, IconEdit2, IconX, IconCheck, Input, InputGroup, Select, Popover } from 'sancho'
import API_ROUTES from '../../../constants/api-routes'
import HTTP_STATUS from '../../../constants/http-status'
import ALERTS from '../../../constants/alerts'
import TEXT from '../../../constants/text-es';
import AuthHelpers from '../../../helpers/auth-helpers'

const AreasTab = (props) => {

  const [areas, setAreas] = useState([]),
        [showAreaForm, setShowAreaForm] = useState(false),
        [creatingArea, setCreatingArea] = useState(false),
        [areaId, setAreaId] = useState(null),
        [areaName, setAreaName] = useState(''),
        [teachers, setTeachers] = useState([]),
        [areaTeacher, setAreaTeacher] = useState(-1),
        [areaNameError, setAreaNameError] = useState(null)

  const toast = useToast()
  
  const tabStyle = css`
    padding: 1rem;
  `,
  buttonStyle = {
    marginTop: '1rem'
  }

  const getAreas = () => {
    Axios.get(API_ROUTES.AREAS)
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setAreas(response.data)
      else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(!response)
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const getTeachers = () => {
    Axios.get(API_ROUTES.ADMINS.TEACHERS, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setTeachers(response.data)
      else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(!response)
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  useEffect(() => {
    if(props.active) {
      getAreas()
      getTeachers()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.active])

  const handleAreaAdd = () => {
    setShowAreaForm(true)
    setCreatingArea(true)
  }

  const handleAreaEdit = (area) => {
    setShowAreaForm(true)
    setCreatingArea(false)
    setAreaId(area.id)
    setAreaName(area.name)
    setAreaTeacher(area.id_teacher || -1)
    setAreaNameError(null)
  }

  const isDataValid = () => {
    if(areaName) {
      if(areaName.length > 0 && areaName.length < 256) {
        if(!areas.some((area) => (creatingArea && area.name.toLowerCase() === areaName.toLowerCase())))
          return true
        else
          setAreaNameError(TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.FIELDS.NAME.ERRORS.UNIQUENESS)
      } else
        setAreaNameError(TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.FIELDS.NAME.ERRORS.FORMAT)
    } else
      setAreaNameError(TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.FIELDS.NAME.ERRORS.PRESENCE)
  }

  const handleAreaCreate = () => {
    if(isDataValid()) {
      var dataToSend = {
        name: areaName
      }
      console.log(areaTeacher)
      if(areaTeacher > -1)
        dataToSend.id_teacher = areaTeacher
      Axios.post(API_ROUTES.ADMINS.AREAS, dataToSend, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
      .then((response) => {
        if(response.status === HTTP_STATUS.OK) {
          let newAreasArr = [...areas],
              newArea = {
                id: response.data.id_area,
                name: areaName
              }
          if(areaTeacher > -1)
            newArea.id_teacher = areaTeacher
          newAreasArr.push(newArea)
          setAreas(newAreasArr)
          setAreaId(null)
          setAreaName('')
          setAreaTeacher(-1)
          setCreatingArea(false)
          setShowAreaForm(false)
          toast(ALERTS.AREAS.SUCCESS_CREATE)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
      .catch((err) => {
        console.error(err)
        let response = err.response
        if(response) {
          if(response.status === HTTP_STATUS.UNAUTHORIZED) {
            AuthHelpers.discardTokenAndRedirect(props, toast)
            toast(ALERTS.GENERAL.UNAUTHORIZED)
          } else
            toast(ALERTS.GENERAL.OUR_ERROR)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
    }
  }

  const handleAreaCancel = () => {
    setAreaName('')
    setAreaId(null)
    setAreaTeacher(-1)
    setAreaNameError(null)
    setShowAreaForm(false)
  }

  const handleAreaDelete = () => {
    alert('próximamente')
  }

  const handleAreaEditConfirm = () => {
    if(isDataValid()) {
      var originalAreaIndex = areas.findIndex((a) => (a.id === areaId)),
          dataToSend = {
            id: areaId
          }
      if(areaName.toLowerCase() !== areas[originalAreaIndex].name.toLowerCase())
        dataToSend.name = areaName
      if(areaTeacher !== areas[originalAreaIndex].id_teacher)
        dataToSend.id_teacher = areaTeacher > -1 ? areaTeacher : null
      Axios.patch(API_ROUTES.ADMINS.AREAS, dataToSend, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
      .then((response) => {
        if(response.status === HTTP_STATUS.OK) {
          if(dataToSend.name)
            areas[originalAreaIndex].name = areaName
          if(dataToSend.id_teacher)
            areas[originalAreaIndex].id_teacher = areaTeacher
          setAreaId(null)
          setAreaName('')
          setAreaTeacher(-1)
          setShowAreaForm(false)
          toast(ALERTS.AREAS.SUCCESS_EDIT)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
      .catch((err) => {
        console.error(err)
        let response = err.response
        if(response) {
          if(response.status === HTTP_STATUS.UNAUTHORIZED) {
            AuthHelpers.discardTokenAndRedirect(props, toast)
            toast(ALERTS.GENERAL.UNAUTHORIZED)
          } else
            toast(ALERTS.GENERAL.OUR_ERROR)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
    }
  }

  const handleAreaNameChange = (ev) => {
    setAreaName(ev.currentTarget.value)
    setAreaNameError(null)
  }

  const handleAreaTeacherChange = (ev) => {
    setAreaTeacher(Number(ev.currentTarget.value))
  }

  const getTeacherOptions = () => {
    return teachers.map((teacher) => (
      <option key={teacher.id} value={teacher.id}>{teacher.fullname}</option>
    ))
  }

  const getAreaDropdownMenu = (area) => {
    return(
      <MenuList>
        <MenuItem contentBefore={<IconEdit2/>} onClick={() => handleAreaEdit(area)}>
          {TEXT.HOME.VIEWS.ADMIN.AREAS.DROPDOWN_MENU.EDIT}
        </MenuItem>
        <MenuItem contentBefore={<IconX color='red'/>} onClick={() => handleAreaDelete(area)}>
          {TEXT.HOME.VIEWS.ADMIN.AREAS.DROPDOWN_MENU.DELETE}
        </MenuItem>
      </MenuList>
    )
  }

  const getAreaRows = () => {
    if(areas.length > 0)
      return areas.map((area) => {
        let teacher = teachers.find((teacher) => (teacher.id === area.id_teacher))
        return(
          <TableRow key={area.id}>
            <TableCell>{area.name}</TableCell>
            <TableCell align='right'>
              {teacher ? teacher.fullname : <em>{TEXT.HOME.VIEWS.ADMIN.AREAS.TABLE.UNASSIGNED_TEACHER}</em>}
            </TableCell>
            <TableCell align='right'>
              <Popover content={getAreaDropdownMenu(area)}>
                <IconButton variant='ghost' icon={<IconMoreVertical/>} label={TEXT.HOME.VIEWS.ADMIN.AREAS.DROPDOWN_MENU.TITLE}/>
              </Popover>
            </TableCell>
          </TableRow>
        )
      })
    else
      return(
        <TableRow>
          <TableCell colSpan={3}>{TEXT.HOME.VIEWS.ADMIN.AREAS.TABLE.NO_AREAS_FOUND}</TableCell>
        </TableRow>
      )
  }

  const getAreaForm = () => {
    if(showAreaForm)
      return(
        <React.Fragment>
          <InputGroup label={TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.FIELDS.NAME.LABEL} error={areaNameError}>
            <Input placeholder={TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.FIELDS.NAME.PLACEHOLDER} value={areaName} onChange={handleAreaNameChange}/>
          </InputGroup>
          <InputGroup label={TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.FIELDS.TEACHER.LABEL}>
            <Select value={areaTeacher} onChange={handleAreaTeacherChange}>
              <option value={-1}>{TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.FIELDS.TEACHER.LABEL}</option>
              {getTeacherOptions()}
            </Select>
          </InputGroup>
          <Button variant='outline' block css={buttonStyle} intent='success' iconBefore={<IconCheck/>} onClick={creatingArea ? handleAreaCreate : handleAreaEditConfirm}>
            {creatingArea ? TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.ADD_BUTTON : TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.EDIT_BUTTON}
          </Button>
          <Button variant='outline' block css={buttonStyle} onClick={handleAreaCancel}>
            {TEXT.HOME.VIEWS.ADMIN.AREAS.FORM.CANCEL_BUTTON}
          </Button>
        </React.Fragment>
      )
    else
      return(
        <Button variant='outline' block css={buttonStyle} intent='primary' iconBefore={<IconPlus/>} onClick={handleAreaAdd}>
          {TEXT.HOME.VIEWS.ADMIN.AREAS.ADD_BUTTON}
        </Button>
      )
  }

  if(props.active)
    return(
      <TabPanel id="areas" className="Tab-panel" css={tabStyle}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.AREAS.TABLE.HEADERS.NAME}</TableCell>
              <TableCell align='right'>{TEXT.HOME.VIEWS.ADMIN.AREAS.TABLE.HEADERS.TEACHER}</TableCell>
              <TableCell align='right'></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {getAreaRows()}
          </TableBody>
        </Table>
        {getAreaForm()}
      </TabPanel>
    )
  else
    return null

}

export default withRouter(AreasTab)