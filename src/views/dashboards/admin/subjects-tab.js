/** @jsx jsx */
import { useState, useEffect } from 'react'
import { jsx, css } from '@emotion/core'
import {withRouter} from 'react-router-dom'
import { useToast, TabPanel, Table, TableHead, TableRow, TableCell, TableBody, Button, IconButton, IconMoreVertical, Dialog, CloseButton, IconChevronRight, IconChevronDown, IconEdit2, IconCheck, IconX, IconPlus, InputGroup, Input, MenuList, MenuItem, Popover, Select, IconMinus } from 'sancho'
import TEXT from '../../../constants/text-es'
import Axios from 'axios';
import API_ROUTES from '../../../constants/api-routes';
import HTTP_STATUS from '../../../constants/http-status';
import ALERTS from '../../../constants/alerts';
import AuthHelpers from '../../../helpers/auth-helpers';

const SubjectsTab = (props) => {

  const [subjects, setSubjects] = useState([]),
        [areas, setAreas] = useState([]),
        [modalsOpen, setModalsOpen] = useState([]),
        [topicsTrees, setTopicsTrees] = useState([]),
        [selectedSubjectIndex, setSelectedSubjectIndex] = useState(null),
        [selectedAreaIndex, setSelectedAreaIndex] = useState(-1),
        [selectedTopic, setSelectedTopic] = useState(null),
        [newTopicName, setNewTopicName] = useState(''),
        [newSubjectName, setNewSubjectName] = useState(''),
        [newSubjectNameError, setNewSubjectNameError] = useState(null),
        [selectedAreaError, setSelectedAreaError] = useState(null),
        [newTopicNameError, setNewTopicNameError] = useState(null),
        [editMode, setEditMode] = useState(false),
        [addingNewTopic, setAddingNewTopic] = useState(false),
        [editingTopic, setEditingTopic] = useState(false),
        [topicsWereChanged, setTopicsWereChanged] = useState(false),
        [savingSubject, setSavingSubject] = useState(false),
        [addingSubject, setAddingSubject] = useState(false),
        [priorsWereChanged, setPriorsWereChanged] = useState(false)

  const toast = useToast()
  
  const tabStyle = css`
    padding: 1rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  `,
  dialogMainDivStyle = css`
    padding: 1rem;
    overflow-y: auto;
    max-height: 80vh;
  `,
  subjectAreaNameStyle = css`
    color: gray;
    font-size: 60%;
  `,
  topicUlStyle = css`
    list-style-type: none;
  `,
  topicLiStyle = css`
    display: flex;
    align-items: center;
  `,
  dialogTopicFormDiv = css`
    display: flex; justify-content: space-between;
  `,
  dialogTopicFormInput = css`
    width: auto;
    flex: 2;
  `,
  buttonStyle = css`
    margin-top: 1rem;
  `

  const getSubjects = () => {
    Axios.get(API_ROUTES.SUBJECTS, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        let newSubjects = response.data === 404 ? [] : response.data,
            newModalsOpen = new Array(newSubjects.length).fill(false)
        setSubjects(newSubjects)
        setModalsOpen(newModalsOpen)
        buildTopicsTreeForAllSubjects(newSubjects)
        .then((result) => {
          setTopicsTrees(result)
        })
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const getAreas = () => {
    Axios.get(API_ROUTES.AREAS)
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setAreas(response.data)
      else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(!response)
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  useEffect(() => {
    if(props.active) {
      getAreas()
      getSubjects()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.active])

  const buildTopicsTreeForAllSubjects = (newSubjects) => {
    var newTopicsTrees = []
    return new Promise((resolve) => {
      newSubjects.forEach((subject) => {
        buildTopicsTree(subject.topics).then((result) => {
          newTopicsTrees.push(result)
        })
      })
      resolve(newTopicsTrees)
    })
  }

  const buildTopicsTree = (topicsArr) => {
    return new Promise((resolve, reject) => {
      var topicsTree = [],
        index = 0
      // Extract root nodes from tree
      while(index < topicsArr.length) {
        var topic = topicsArr[index]
        if(!topic.parent_id) {
          topic.collapsed = true
          topicsTree.push(topic)
          topicsArr.splice(index, 1)
          continue
        }
        index++
      }
      topicsTree.forEach(topic => {
        buildTopicsSubTree(topicsArr, topic.id).then((result) => {
          topic.children = result
        })
      })
      return resolve(topicsTree)
    })
  }

  const buildTopicsSubTree = (topicsArr, parentId) => {
    return new Promise((resolve, reject) => {
      var topicsSubTree = [],
        index = 0,
        topic = null
      const getResult = (result) => {
        topic.children = result
      }
      while(topicsArr.length > 0 && index < topicsArr.length) {
        topic = topicsArr[index]
        if(topic.parent_id === parentId) {
          topic.collapsed = true
          topicsArr.splice(index, 1)
          buildTopicsSubTree(topicsArr, topic.id).then(getResult)
          topicsSubTree.push(topic)
          continue
        }
        index++
      }
      return resolve(topicsSubTree)
    })
  }
  
  const openModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = true
    setModalsOpen(newModalsOpen)
    setSelectedSubjectIndex(index)
  }

  const closeModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = false
    setModalsOpen(newModalsOpen)
    setSelectedSubjectIndex(null)
    setEditMode(false)
    if(addingSubject)
      cancelNewSubject()
    console.log('puto')
  }

  const collapseOrExpandTopic = (topic) => {
    topic.collapsed = !topic.collapsed
    updateSubjectTopicTree()
    setSelectedTopic(topic)
  }

  const updateSubjectTopicTree = () => {
    let newTopicsTrees = [...topicsTrees]
    setTopicsTrees(newTopicsTrees)
  }

  const enterEditMode = () => {
    setEditMode(true)
    setNewSubjectName(subjects[selectedSubjectIndex].name)
  }

  const deleteTopics = (topicsArr) => {
    var index = 0
    while(index < topicsArr.length) {
      if(topicsArr[index].delete_me) {
        topicsArr.splice(index, 1)
        continue
      } else if(topicsArr[index].children && topicsArr[index].children.length > 0)
        deleteTopics(topicsArr[index].children)
      index++
    }
  }

  const unmarkTopicsForDelete = (topicsArr) => {
    var index = 0
    while(index < topicsArr.length) {
      if(topicsArr[index].delete_me) {
        delete topicsArr[index].delete_me
        continue
      }
      index++
    }
  }

  const removeCancelledNewTopics = (topicsArr) => {
    var index = 0
    while(index < topicsArr.length) {
      if(topicsArr[index].im_new) {
        topicsArr.splice(index, 1)
        continue
      }
      index++
    }
  }

  const cancelEditMode = () => {
    unmarkTopicsForDelete(topicsTrees[selectedSubjectIndex])
    removeCancelledNewTopics(topicsTrees[selectedSubjectIndex])
    setEditMode(false)
    setSelectedTopic(null)
    setAddingNewTopic(false)
    setNewTopicName('')
    if(addingSubject)
      cancelNewSubject()
  }

  const handleSubjectNameChange = (ev) => {
    let newName = ev.target.value
    setNewSubjectName(newName)
    setNewSubjectNameError(null)
  }

  const saveSubjectChanges = (index) => {
    let numberOfFields = 2
    var dataToSend = {
          id: subjects[selectedSubjectIndex].id
        },
        validFieldsNumber = 0

    // Validate data
    if(newSubjectName !== subjects[selectedSubjectIndex].name) {
      if(newSubjectName.length < 256) {
        if(newSubjectName.length > 0) {
          if(newSubjectName !== subjects[selectedSubjectIndex].name) {
            dataToSend.name = newSubjectName
            validFieldsNumber++
          }
        } else
          setNewSubjectNameError(TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.SUBJECT_NAME_FIELD.ERRORS.PRESENCE)
      } else
        setNewSubjectNameError(TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.SUBJECT_NAME_FIELD.ERRORS.FORMAT)
    } else
      validFieldsNumber++

    if(addingSubject) {
      if(selectedAreaIndex > -1) {
        dataToSend.area_id = areas[selectedAreaIndex].id
        validFieldsNumber++
      }
      else
        setSelectedAreaError(TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.AREA_FIELD.ERRORS.PRESENCE)
    } else
      validFieldsNumber++

    if(validFieldsNumber < numberOfFields)
      return

    if(priorsWereChanged)
      dataToSend.priors = subjects[selectedSubjectIndex].priors

    // Turn spinner on
    setSavingSubject(true)

    if(topicsWereChanged) {
      deleteTopics(topicsTrees[selectedSubjectIndex])
      dataToSend.topics = topicsTrees[index]
    }

    if(dataToSend.name || dataToSend.topics || dataToSend.priors) {
      let action = addingSubject ? Axios.post : Axios.patch
      action(API_ROUTES.ADMINS.SUBJECTS, dataToSend, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
      .then((response) => {
        if(response.status === HTTP_STATUS.OK) {
          let newSubjects = [...subjects]
          subjects[selectedSubjectIndex].name = newSubjectName
          setSubjects(newSubjects)
          setEditMode(false)
          setNewSubjectName('')
          setSelectedTopic(null)
          setTopicsWereChanged(false)
          setPriorsWereChanged(false)
          setNewSubjectNameError(null)
          setNewTopicNameError(null)
          if(addingSubject) {
            setAddingSubject(false)
            subjects[selectedSubjectIndex].area = {
              id: areas[selectedAreaIndex].id,
              name: areas[selectedAreaIndex].name
            }
            toast(ALERTS.SUBJECTS.SUCCESS_CREATE)
          } else
            toast(ALERTS.SUBJECTS.SUCCESS_EDIT)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
      .catch((err) => {
        console.error(err)
        let response = err.response
        if(response) {
          if(response.status === HTTP_STATUS.UNAUTHORIZED)
            AuthHelpers.discardTokenAndRedirect(props, toast)
          else if(response.status === HTTP_STATUS.CONFLICT)
            toast(ALERTS.SUBJECTS.NAME_IN_USE)
          else
            toast(ALERTS.GENERAL.OUR_ERROR)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
      .finally(() => {
        setSavingSubject(false)
      })
    } else {
      setEditMode(false)
      setNewSubjectName('')
      setAddingNewTopic(false)
      setTopicsWereChanged(false)
      setNewSubjectNameError(null)
      setNewTopicNameError(null)
    }
  }

  const handleNewTopicNameChange = (ev) => {
    let newTopicNewName = ev.target.value
    setNewTopicName(newTopicNewName)
  }

  const addNewTopic = (topic) => {
    setAddingNewTopic(true)
    setSelectedTopic(topic)
  }

  const addNewRootTopic = () => {
    setAddingNewTopic(true)
    setSelectedTopic(null)
  }

  const editTopic = (topic) => {
    setEditingTopic(true)
    setSelectedTopic(topic)
    setNewTopicName(topic.name)
  }

  const markTopicForDelete = (topic) => {
    topic.delete_me = true
    setTopicsWereChanged(true)
    updateSubjectTopicTree()
    if(addingSubject)
      deleteTopics(topicsTrees[selectedSubjectIndex])
  }

  const cancelNewTopic = () => {
    setAddingNewTopic(false)
    setSelectedTopic(null)
  }

  const cancelEditTopic = () => {
    setEditingTopic(false)
  }

  const confirmEditTopic = () => {
    if(newTopicName.length > 0) {
      if(newTopicName.length < 256) {
        selectedTopic.name = newTopicName
        setNewTopicName('')
        setEditingTopic(false)
        setSelectedTopic(null)
        setTopicsWereChanged(true)
      } else
        setNewTopicNameError(TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.TOPIC_NAME_FIELD.ERRORS.FORMAT)
    } else
      setNewTopicNameError(TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.TOPIC_NAME_FIELD.ERRORS.PRESENCE)
  }

  const confirmAddTopic = () => {
    if(newTopicName.length > 0) {
      if(newTopicName.length < 256) {
        var newTopic = {
          name: newTopicName,
          children: [],
          new: true
        }
        if(selectedTopic) {
          if(selectedTopic.children)
            selectedTopic.children.push(newTopic)
          else
            selectedTopic.children = [newTopic]
          setSelectedTopic(null)
        } else {
          topicsTrees[selectedSubjectIndex].push(newTopic)
        }
        setNewTopicName('')
        setAddingNewTopic(false)
        setTopicsWereChanged(true)
      } else
        setNewTopicNameError(TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.TOPIC_NAME_FIELD.ERRORS.FORMAT)
    } else
      setNewTopicNameError(TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.TOPIC_NAME_FIELD.ERRORS.PRESENCE)
  }

  const cancelNewSubject = () => {
    let newSubjects = [...subjects],
        newModalsOpen = [...modalsOpen],
        newTopicsTrees = [...topicsTrees]
    newSubjects.pop()
    newModalsOpen.pop()
    newTopicsTrees.pop()
    setModalsOpen(newModalsOpen)
    setTopicsTrees(newTopicsTrees)
    setSubjects(newSubjects)
    setAddingSubject(false)
    setSelectedSubjectIndex(null)
    setEditMode(false)
    setNewSubjectName('')
  }

  const addNewSubject = () => {
    let newSubjects = [...subjects],
        newModalsOpen = [...modalsOpen],
        newTopicsTrees = [...topicsTrees]
    newSubjects.push({
      area_id: -1,
      name: TEXT.HOME.VIEWS.ADMIN.SUBJECTS.SUBJECT_IN_PROGRESS,
      topics: []
    })
    newModalsOpen.push(true)
    newTopicsTrees.push([])
    setModalsOpen(newModalsOpen)
    setTopicsTrees(newTopicsTrees)
    setSubjects(newSubjects)
    setAddingSubject(true)
    setEditMode(true)
    setNewSubjectName('')
    setSelectedSubjectIndex(newSubjects.length - 1)
    setSelectedAreaIndex(-1)
  }

  const handleAreaChange = (ev) => {
    setSelectedAreaIndex(Number(ev.target.value))
    setSelectedAreaError(null)
  }

  const handlePriorChange = (ev) => {
    let newSubjects = [...subjects],
        index = Number(ev.target.value)
    newSubjects[selectedSubjectIndex].priors.push(subjects[index].id)
    setSubjects(newSubjects)
    setPriorsWereChanged(true)
  }

  const removePrior = (index) => {
    let newSubjects = [...subjects]
    newSubjects[selectedSubjectIndex].priors.splice(index, 1)
    setSubjects(newSubjects)
    setPriorsWereChanged(true)
  }

  const changeSelectedSubject = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen.fill(false)
    newModalsOpen[index] = true
    setModalsOpen(newModalsOpen)
    setSelectedSubjectIndex(index)
  }

  const getSubjectTopicTree = (topicsArr, subjectIndex) => {
    return topicsArr.map((topic, index) => {
      return(
        <ul key={index} css={topicUlStyle}>
          {
            !topic.delete_me &&
            <li>
              <div css={topicLiStyle}>
                <Button
                  variant='ghost'
                  intent={selectedTopic && selectedTopic.id === topic.id && editMode ? 'primary' : 'none'}
                  iconBefore={
                    topic.children && topic.children.length > 0 ? (
                      topic.collapsed ? <IconChevronDown/> : <IconChevronRight/>
                    ) : <IconMinus/>
                  }
                  onClick={() => collapseOrExpandTopic(topic)}
                >
                  {topic.name}
                </Button>
                {
                  editMode &&
                  <Popover content={getTopicDrowdownMenu(topic, subjectIndex)}>
                    <IconButton
                      variant='ghost'
                      icon={<IconMoreVertical/>}
                      label=''
                    />
                  </Popover>
                }
              </div>
              {topic.collapsed && topic.children && topic.children.length > 0 && getSubjectTopicTree(topic.children)}
            </li>
          }
        </ul>
      )
    })
  }

  const getTopicDrowdownMenu = (topic, subjectIndex) => {
    return(
      <MenuList>
        <MenuItem contentBefore={<IconPlus color='green'/>} onClick={() => addNewTopic(topic)}>
          {TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.DROPDOWN_MENU.ADD_TOPIC}
        </MenuItem>
        <MenuItem contentBefore={<IconEdit2/>} onClick={() => editTopic(topic)}>
          {TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.DROPDOWN_MENU.EDIT_TOPIC}
        </MenuItem>
        <MenuItem contentBefore={<IconX color='red'/>} onClick={() => markTopicForDelete(topic)}>
          {TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.DROPDOWN_MENU.DELETE_TOPIC}
        </MenuItem>
      </MenuList>
    )
  }

  const getAreasRows = () => {
    return areas.map((area, index) => (<option key={index} value={index}>{area.name}</option>))
  }

  const getPriorsOptions = () => {
    return subjects.slice(0, -2).map((subject, index) => (
      <option key={index} value={index}>{subject.name}</option>
    ))
  }

  const getPriorsRows = () => {
    let priors = selectedSubjectIndex ? subjects[selectedSubjectIndex].priors : null
    if(priors)
      return priors.map((prior, index) => {
        let priorSubjectIndex = subjects.findIndex((subject) => (subject.id === prior))
        if(prior)
          return(
            <Button
              variant='outline'
              intent='primary'
              css={css`margin-right: 1rem; margin-bottom: 1rem; margin-top: 1rem;`}
              iconAfter={editMode ? <IconX/> : null}
              onClick={() => (editMode ? removePrior(index) : changeSelectedSubject(priorSubjectIndex))}
            >
              {subjects[priorSubjectIndex].name}
            </Button>
          )
        else
          return null
      })
    else
      return null
  }

  const getSubjectDialog = (subject, index) => {
    return(
      <Dialog isOpen={modalsOpen[index]} onRequestClose={() => closeModal(index)}>
        <div css={dialogMainDivStyle}>
          {
            !addingSubject &&
            <div style={{display: 'flex', justifyContent: 'flex-end'}}>
              <CloseButton onClick={() => closeModal(index)}/>
            </div>
          }
          <div>
            {
              subject.area && !editMode &&
              <div css={subjectAreaNameStyle}>{subject.area.name + ' >'}</div>
            }
            <div css={css`display: flex; align-items: flex-end; justify-content: space-between;`}>
              {
                editMode
                ? <InputGroup
                    label={addingSubject ? TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.SUBJECT_NAME_FIELD.LABEL : ''}
                    hideLabel={!addingSubject}
                    error={newSubjectNameError}
                  >
                    <Input
                        placeholder={TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.SUBJECT_NAME_FIELD.LABEL}
                        value={newSubjectName}
                        onChange={handleSubjectNameChange}
                        css={dialogTopicFormInput}
                      />
                  </InputGroup> 
                : subject.name
              }
              <div>
                {
                  editMode &&
                  <IconButton 
                    variant='ghost'
                    intent='danger'
                    icon={<IconX/>}
                    label=''
                    onClick={cancelEditMode}
                  />
                }
                <IconButton 
                  variant='ghost'
                  intent={editMode ? 'success' : 'primary'}
                  icon={editMode ? <IconCheck/> : <IconEdit2/>}
                  label=''
                  loading={savingSubject}
                  disabled={addingSubject && topicsTrees[selectedSubjectIndex].length === 0}
                  onClick={editMode ? () => saveSubjectChanges(index) : enterEditMode}
                />
              </div>
            </div>
            {
              editMode && addingSubject &&
              <InputGroup
                label={TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.AREA_FIELD.LABEL}
                css={css`margin-bottom: 1rem;`}
                error={selectedAreaError}
              >
                <Select value={selectedAreaIndex} onChange={handleAreaChange}>
                  <option value={-1}>{TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.AREA_FIELD.LABEL}</option>
                  {getAreasRows()}
                </Select>
              </InputGroup> 
            }
            {
              editMode &&
              <InputGroup
                label={TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.PRIORS_FIELD.LABEL}
                css={css`margin-bottom: 1rem;`}
              >
                <Select onChange={handlePriorChange}>
                  <option value={-1}>{TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.PRIORS_FIELD.LABEL}</option>
                  {getPriorsOptions()}
                </Select>
              </InputGroup> 
            }
          </div>
          { getPriorsRows() }
          {
            topicsTrees[index].length > 0
            ? getSubjectTopicTree(topicsTrees[index], index)
            : <em
                css={css`display: block; font-size: 80%; padding: 1.5rem 1rem 1rem; color: gray;`}
              >
                {TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.NO_TOPICS}
              </em>
          }
          {
            editMode &&
            <Button
              variant='outline'
              intent='success'
              block
              css={buttonStyle}
              iconBefore={<IconPlus/>}
              onClick={addNewRootTopic}
            >
              {TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.NEW_ROOT_TOPIC_BUTTON}
            </Button>
          }
          {
            editMode && (addingNewTopic || editingTopic) &&
            <InputGroup label={TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.TOPIC_NAME_FIELD.LABEL} error={newTopicNameError}>
              <div css={dialogTopicFormDiv}>
                <Input
                  placeholder={TEXT.HOME.VIEWS.ADMIN.SUBJECTS.DIALOG.TOPIC_NAME_FIELD.PLACEHOLDER}
                  value={newTopicName}
                  onChange={handleNewTopicNameChange}
                  css={dialogTopicFormInput}
                />
                <IconButton 
                  variant='ghost'
                  intent='success'
                  icon={<IconCheck/>}
                  label=''
                  onClick={editingTopic ? confirmEditTopic : confirmAddTopic}
                />
                <IconButton 
                  variant='ghost'
                  intent='danger'
                  icon={<IconX/>}
                  label=''
                  onClick={editingTopic ? cancelEditTopic : cancelNewTopic}
                />
              </div>
            </InputGroup>
          }
        </div>
      </Dialog>
    )
  }

  const getSubjectsRows = () => {
    if(subjects.length > 0 && modalsOpen.length === subjects.length && topicsTrees.length === subjects.length)
      return subjects.map((subject, index) => {
        return(
          <TableRow key={index}>
            <TableCell>{subject.area ? subject.area.name : '-'}</TableCell>
            <TableCell>{subject.name}</TableCell>
            <TableCell align='right'>
              <IconButton variant='ghost' icon={<IconMoreVertical/>} label={TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.GHOST_BUTTON_LABEL} onClick={() => openModal(index)}/>
              {getSubjectDialog(subject, index)}
            </TableCell>
          </TableRow>
        )
      })
    else
      return(
        <TableRow>
          <TableCell colSpan={3}>{TEXT.HOME.VIEWS.ADMIN.SUBJECTS.TABLE.NO_SUBJECTS_FOUND}</TableCell>
        </TableRow>
      )
  }

  if(props.active)
    return(
      <TabPanel id="admissions" className="Tab-panel" css={tabStyle}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.SUBJECTS.TABLE.HEADERS.AREA}</TableCell>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.SUBJECTS.TABLE.HEADERS.NAME}</TableCell>
              <TableCell align='right'></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {getSubjectsRows()}
          </TableBody>
        </Table>
        <Button
          variant='outline'
          intent='primary'
          block
          css={buttonStyle}
          iconBefore={<IconPlus/>}
          onClick={addNewSubject}
        >
          {TEXT.HOME.VIEWS.ADMIN.SUBJECTS.ADD_SUBJECT_BUTTON}
        </Button>
      </TabPanel>
    )
  else
    return null

}

export default withRouter(SubjectsTab)