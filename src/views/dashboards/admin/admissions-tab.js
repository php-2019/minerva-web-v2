/** @jsx jsx */
import React, { useState, useEffect } from 'react'
import { jsx, css } from '@emotion/core'
import {withRouter} from 'react-router-dom'
import { useToast, TabPanel, Table, TableHead, TableRow, TableCell, TableBody, Collapse, Button, InputGroup, Check, IconButton, IconMoreVertical, Dialog, IconUserPlus, IconUserMinus, CloseButton } from 'sancho'
import TEXT from '../../../constants/text-es'
import Axios from 'axios';
import API_ROUTES from '../../../constants/api-routes';
import HTTP_STATUS from '../../../constants/http-status';
import ALERTS from '../../../constants/alerts';
import AuthHelpers from '../../../helpers/auth-helpers';
import USER_TYPES from '../../../constants/user-types'
import GeneralHelpers from '../../../helpers/general-helpers';

const AdmissionsTab = (props) => {

  const [admissions, setAdmissions] = useState([]),
        [showAdvancedOptions, setShowAdvancedOptions] = useState(false),
        [admissionPeriod, setAdmissionPeriod] = useState(false),
        [modalsOpen, setModalsOpen] = useState([]),
        [selectedAdmissions, setSelectedAdmissions] = useState([]),
        [allSelected, setAllSelected] = useState(false)

  const toast = useToast()
  
  const tabStyle = css`
    padding: 1rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  `,
  buttonStyle = css`
    margin-top: 1rem;
  `,
  advancedOptionsDivStyle = css`
    padding: 1rem;
  `,
  phonesListStyle = css`
    float: right;
    text-align: left;
  `,
  dialogMainDivStyle = css`
    padding: 1rem;
  `,
  dialogButtonStyle = css`
    width: 49%;
  `,
  dialogButtonsDivStyle = css`
    display: flex;
    justify-content: space-between;
    padding-top: 1rem;
  `

  const getAdmissions = () => {
    Axios.get(API_ROUTES.ADMINS.UNAPPROVED, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        let newAdmissions = response.data === 404 ? [] : response.data,
            newModalsOpen = new Array(newAdmissions.length).fill(false),
            newSelectedAdmissions = new Array(newAdmissions.length).fill(false)
        setAdmissions(newAdmissions)
        setModalsOpen(newModalsOpen)
        setSelectedAdmissions(newSelectedAdmissions)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const getAdmissionPeriodState = () => {
    Axios.get(API_ROUTES.USERS.ADMISSIONS, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setAdmissionPeriod(response.data)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  useEffect(() => {
    if(props.active) {
      getAdmissionPeriodState()
      getAdmissions()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.active])

  const changeUsersState = (newState, individualUserId) => {
    var users = [],
        dataToSend = {
          state: newState
        }
    if(individualUserId)
      users.push(individualUserId)
    else
      users = selectedAdmissions.map((admitted, index) => (
        admitted ? admissions[index].id : null)
      ).filter((elem) => (elem))
    dataToSend.ids = users
    Axios.patch(API_ROUTES.ADMINS.APPROVE, dataToSend, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        getAdmissions()
        toast(ALERTS.ADMISSIONS.SUCCESS_CHANGE_STATE)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const toggleAdvancedOptions = () => {
    setShowAdvancedOptions(!showAdvancedOptions)
  }

  const toggleAdmissionPeriod = (ev) => {
    ev.stopPropagation()
    let result = !admissionPeriod
    Axios.patch(API_ROUTES.ADMINS.ADMISSIONS, result, {headers: {Authorization: AuthHelpers.getToken(props, toast)}})
    .then((response) => {
      if(response.status === HTTP_STATUS.OK) {
        toast(ALERTS.ADMISSIONS.SUCCESS_CHANGE_STATE)
        setAdmissionPeriod(result)
      }
      else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
    .catch((err) => {
      console.error(err)
      let response = err.response
      if(response) {
        if(response.status === HTTP_STATUS.UNAUTHORIZED)
          AuthHelpers.discardTokenAndRedirect(props, toast)
        else
          toast(ALERTS.GENERAL.OUR_ERROR)
      } else
        toast(ALERTS.GENERAL.OUR_ERROR)
    })
  }

  const selectAllAdmissions = () => {
    let newSelectedAdmissions = [...selectedAdmissions],
        result = !allSelected
    newSelectedAdmissions.fill(result)
    setSelectedAdmissions(newSelectedAdmissions)
    setAllSelected(result)
  }

  const handleAdmissionSelectionChange = (index) => {
    let newSelectedAdmissions = [...selectedAdmissions],
        result = !selectedAdmissions[index]
    newSelectedAdmissions[index] = result
    setSelectedAdmissions(newSelectedAdmissions)
    if(!result)
      setAllSelected(false)
  }
  
  const openModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = true
    setModalsOpen(newModalsOpen)
  }

  const closeModal = (index) => {
    let newModalsOpen = [...modalsOpen]
    newModalsOpen[index] = false
    setModalsOpen(newModalsOpen)
  }

  const getAdmissionRows = () => {
    if(admissions.length > 0 && modalsOpen.length === admissions.length && selectedAdmissions.length === admissions.length) {
      console.log(admissions)
      return admissions.map((admission, index) => {
        return(
          <TableRow key={admission.id}>
            <TableCell>
              <Check label='' checked={selectedAdmissions[index]} onChange={() => handleAdmissionSelectionChange(index)}/>
            </TableCell>
            <TableCell>
              {
                admission.type === USER_TYPES.STUDENT
                ? TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.TABLE.STUDENT_TYPE
                : TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.TABLE.TEACHER_TYPE
              }
            </TableCell>
            <TableCell>{`${admission.surname}, ${admission.name}`}</TableCell>
            <TableCell align='right'>
              <IconButton variant='ghost' icon={<IconMoreVertical/>} label={TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.GHOST_BUTTON_LABEL} onClick={() => openModal(index)}/>
              <Dialog isOpen={modalsOpen[index]} mobileFullscreen onRequestClose={() => closeModal(index)}>
                <div css={dialogMainDivStyle}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.HEADER}</TableCell>
                        <TableCell align='right'><CloseButton onClick={() => closeModal(index)}/></TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.DOCUMENT}</TableCell>
                        <TableCell align='right'>{admission.document}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.FULL_NAME}</TableCell>
                        <TableCell align='right'>{`${admission.surname}, ${admission.name}`}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.USERNAME}</TableCell>
                        <TableCell align='right'>{admission.username}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.EMAIL}</TableCell>
                        <TableCell align='right'>{admission.email}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.BIRTHDAY}</TableCell>
                        <TableCell align='right'>{GeneralHelpers.dateToHumanReadableStr(new Date(Date.parse(admission.birthday.date)))}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.PHONES}</TableCell>
                        <TableCell align='right'>
                          <ul css={phonesListStyle}>
                            {
                              admission.phones.map((phone, index) => (
                                <li key={index} >{phone}</li>
                              ))
                            }
                          </ul>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.TABLE.REGISTER_DATE}</TableCell>
                        <TableCell align='right'>{GeneralHelpers.dateToHumanReadableStr(new Date(Date.parse(admission.registeredSince.date)))}</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                  <div css={dialogButtonsDivStyle}>
                    <Button intent='danger' iconBefore={<IconUserMinus/>} css={dialogButtonStyle} onClick={() => changeUsersState(false, admission.id)}>
                      {TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.REJECT}
                    </Button>
                    <Button intent='success' iconAfter={<IconUserPlus/>} css={dialogButtonStyle} onClick={() => changeUsersState(true, admission.id)}>
                      {TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.INFO_DIALOG.APPROVE}
                    </Button>
                  </div>
                </div>
              </Dialog>
            </TableCell>
          </TableRow>
        )
      })
    } else
      return(
        <TableRow>
          <TableCell colSpan={3}>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.TABLE.NO_ADMISSIONS_FOUND}</TableCell>
        </TableRow>
      )
  }

  const getAdmissionPeriodCheckmarkText = () => {
    if(admissionPeriod)
      return TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.ADVANCED_OPTIONS.ADMISSION_PERIOD_CHECKMARK.ON
    else
      return TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.ADVANCED_OPTIONS.ADMISSION_PERIOD_CHECKMARK.OFF
  }

  if(props.active)
    return(
      <TabPanel id="admissions" className="Tab-panel" css={tabStyle}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><Check label='' checked={allSelected} onChange={selectAllAdmissions}/></TableCell>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.TABLE.HEADERS.TYPE}</TableCell>
              <TableCell>{TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.TABLE.HEADERS.NAME}</TableCell>
              <TableCell align='right'></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {getAdmissionRows()}
          </TableBody>
        </Table>
        {
          selectedAdmissions.includes(true) &&
          <React.Fragment>
            <Button block intent='danger' iconBefore={<IconUserMinus/>} css={buttonStyle} onClick={() => changeUsersState(false)}>
              {TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.BATCH_REJECT}
            </Button>
            <Button block intent='success' iconBefore={<IconUserPlus/>} css={buttonStyle} onClick={() => changeUsersState(true)}>
              {TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.BATCH_APPROVE}
            </Button>
          </React.Fragment>
        }
        <Button variant='ghost' intent='primary' block css={buttonStyle} onClick={toggleAdvancedOptions}>
          {TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.ADVANCED_OPTIONS.BUTTON}
        </Button>
        <Collapse id="collapse" show={showAdvancedOptions} css={advancedOptionsDivStyle}>
          <InputGroup label={TEXT.HOME.VIEWS.ADMIN.ADMISSIONS.ADVANCED_OPTIONS.ADMISSION_PERIOD_CHECKMARK.LABEL}>
            <Check label={getAdmissionPeriodCheckmarkText()} checked={admissionPeriod} onChange={toggleAdmissionPeriod}/>
          </InputGroup>
        </Collapse>
      </TabPanel>
    )
  else
    return null

}

export default withRouter(AdmissionsTab)