/** @jsx jsx */
import React, { useState } from 'react'
import { jsx, css } from '@emotion/core'
import { Layer, Tab, Tabs, GestureView } from 'sancho'
import { isMobile } from 'react-device-detect';
import TEXT from '../../../constants/text-es';
import AreasTab from './areas-tab';
import AdmissionsTab from './admissions-tab'
import SubjectsTab from './subjects-tab'
import ClassroomsTab from './classrooms-tab'
import CalendarTab from './calendar-tab'
import ProfileTab from '../profile-tab'

const DashboardStudent = (props) => {

  const [value, setValue] = useState(0)
  
  const layerStyle = {
    padding: '1.5rem',
    overflowY: 'auto',
    // maxHeight: '60vh'
  },
  mainDivStyle = {
    padding: '1rem',
    fontFamily: 'Tiresias',
    overflowY: 'auto',
    maxHeight: '60vh'
  },
  tabHeaderStyle = css`
    & > span {
      font-size: 120%;
    }
  `
  
  const getTabHeaders = () => {
    return([
      <Tab id="admissions" css={tabHeaderStyle}>{TEXT.HOME.TABS.ADMIN.ADMISSIONS}</Tab>,
      <Tab id="areas" css={tabHeaderStyle}>{TEXT.HOME.TABS.ADMIN.AREAS}</Tab>,
      <Tab id="subjects" css={tabHeaderStyle}>{TEXT.HOME.TABS.ADMIN.SUBJECTS}</Tab>,
      <Tab id="classrooms" css={tabHeaderStyle}>{TEXT.HOME.TABS.ADMIN.CLASSROOMS}</Tab>,
      <Tab id="calendar" css={tabHeaderStyle}>{TEXT.HOME.TABS.ADMIN.CALENDAR}</Tab>,
      <Tab id="profile" css={tabHeaderStyle}>{TEXT.HOME.TABS.GENERAL.PROFILE.TITLE}</Tab>
    ])
  }

  const getTabs = () => {
    return([
      <AdmissionsTab active={value === 0}/>,
      <AreasTab active={value === 1}/>,
      <SubjectsTab active={value === 2}/>,
      <ClassroomsTab active={value === 3}/>,
      <CalendarTab active={value === 4}/>,
      <ProfileTab/>
    ])
  }

  const render = () => (
    <React.Fragment>
      <Tabs
        value={value}
        onChange={i => setValue(i)} children={getTabHeaders()}
      />
      <GestureView value={value} onRequestChange={i => setValue(i)} children={getTabs()}/>
    </React.Fragment>
  )

  if(isMobile)
    return(
      <div css={mainDivStyle}>
        {render()}
      </div>
    )
  else
    return(
      <Layer css={layerStyle}>
        {render()}
      </Layer>
    )

}

export default DashboardStudent