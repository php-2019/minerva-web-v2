/** @jsx jsx */
import { useState, useEffect } from 'react'
import { jsx } from '@emotion/core'
import Login from './login'
import { GestureView } from 'sancho';
import SignUp from './signup';
import Axios from 'axios';
import API_ROUTES from '../constants/api-routes';
import HTTP_STATUS from '../constants/http-status';

const Home = (props) => {

  const [formValue, setFormValue] = useState(0),
        [admissionPeriod, setAdmissionPeriod] = useState(false)

  const handleGoSignUp = () => {
    setFormValue(1)
  }

  const handleReturnLogIn = () => {
    setFormValue(0)
  }

  useEffect(() => {
    Axios.get(API_ROUTES.USERS.ADMISSIONS)
    .then((response) => {
      if(response.status === HTTP_STATUS.OK)
        setAdmissionPeriod(response.data)
    })
    .catch((err) => {
      console.error(err)
    })
  }, [])

  return(
    <GestureView value={formValue} onRequestChange={(i) => (setFormValue(i))}>
      <Login onGoSignup={handleGoSignUp} admissionPeriod={admissionPeriod}/>
      {
        admissionPeriod &&
        <SignUp onReturnLogin={handleReturnLogIn}/>
      }
    </GestureView>
  )
}

export default Home