import React from 'react'
import USER_TYPES from '../constants/user-types'
import DashboardAdmin from './dashboards/admin/dashboard-admin'

const Dashboard = (props) => {

  let userType = Number(localStorage.getItem('minerva-usertype'))

  switch(userType) {
    case USER_TYPES.ADMIN:
      return <DashboardAdmin/>
    case USER_TYPES.STUDENT:
      return null
    case USER_TYPES.TEACHER:
      return null
    default:
      return null
  }

}

export default Dashboard