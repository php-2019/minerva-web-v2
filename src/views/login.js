/** @jsx jsx */
import React, {useState} from 'react'
import {jsx} from '@emotion/core'
import { InputGroup, Input, Button, IconLogIn, useToast, Layer } from 'sancho'
import TEXT from '../constants/text-es'
import Axios from 'axios'
import {withRouter} from 'react-router-dom'
import {isMobile} from 'react-device-detect'
import AuthHelpers from '../helpers/auth-helpers'
import API_ROUTES from '../constants/api-routes'
import HTTP_STATUS from '../constants/http-status'
import ALERTS from '../constants/alerts'
import GeneralHelpers from '../helpers/general-helpers';

const Login = (props) => {

  const [id, setId] = useState(''),
        [password, setPassword] = useState(''),
        [idError, setIdError] = useState(null),
        [passwordError, setPasswordError] = useState(null),
        [connecting, setConnecting] = useState(false),
        [canPressEnter, setCanPressEnter] = useState(true)

  const toast = useToast()

  const layerStyle = {
    margin: '1.5rem',
    padding: '1.5rem',
    fontFamily: 'Tiresias',
    minWidth: '320px'
  },
  mainDivStyle = {
    padding: '1.5rem',
    fontFamily: 'Tiresias',
  },
  buttonStyle = {
    marginTop: '1rem'
  }

  const handleIdChange = (ev) => {
    setId(ev.target.value)
    setIdError(null)
  }

  const handlePasswordChange = (ev) => {
    setPassword(ev.target.value)
    setPasswordError(null)
  }

  const handleEnterKeyDown = (ev) => {
    if(ev.key === 'Enter' && canPressEnter) {
      setCanPressEnter(false)
      handleLoginButtonClick()
    }
  }

  const handleEnterKeyUp = (ev) => {
    if(ev.key === 'Enter')
      setCanPressEnter(true)
  }

  const handleLoginButtonClick = () => {
    if(id && password) {
      setConnecting(true)
      var dataToSend = {
        identifier: id,
        password
      }
      if(GeneralHelpers.isEmail(id))
        dataToSend.type = 'email'
      else if(GeneralHelpers.isCedula(id))
        dataToSend.type = 'document'
      else {
        dataToSend.type = 'username'
        dataToSend.id = '@' + id
      }
      Axios.post(API_ROUTES.USERS.LOGIN, dataToSend)
      .then((response) => {
        setConnecting(false)
        if(response.status === HTTP_STATUS.OK && response.data && response.data.token) {
          localStorage.setItem('minerva-userid', response.data.id)
          localStorage.setItem('minerva-username', response.data.username)
          localStorage.setItem('minerva-usertype', response.data.type)
          localStorage.setItem('minerva-token', response.data.token)
          props.history.push('/')
        } else {
          toast(ALERTS.GENERAL.OUR_ERROR)
        }
      })
      .catch((err) => {
        setConnecting(false)
        console.error(err)
        var response = err.response
        if(response) {
          if(response.status === HTTP_STATUS.UNAUTHORIZED) {
            AuthHelpers.discardTokenAndRedirect(props, toast)
            toast(ALERTS.LOGIN_FAILED)
          } else
            toast(ALERTS.GENERAL.OUR_ERROR)
        } else
          toast(ALERTS.GENERAL.OUR_ERROR)
      })
    } else {
      if(!id)
        setIdError(TEXT.LOGIN.FIELDS.ID.ERROR)
      if(!password)
        setPasswordError(TEXT.LOGIN.FIELDS.PASSWORD.ERROR)
    }
  }

  const handleSignupButtonClick = () => {
    setIdError(null)
    setPasswordError(null)
    props.onGoSignup()
  }

  const render = () => (
    // 
    <React.Fragment>
      <InputGroup label={TEXT.LOGIN.FIELDS.ID.LABEL} error={idError}>
        <Input placeholder={TEXT.LOGIN.FIELDS.ID.PLACEHOLDER} value={id} onChange={handleIdChange} onKeyDown={handleEnterKeyDown} onKeyUp={handleEnterKeyUp}/>
      </InputGroup>
      <InputGroup label={TEXT.LOGIN.FIELDS.PASSWORD.LABEL} error={passwordError}>
        <Input type='password' placeholder={TEXT.LOGIN.FIELDS.PASSWORD.PLACEHOLDER} value={password} onChange={handlePasswordChange} onKeyDown={handleEnterKeyDown} onKeyUp={handleEnterKeyUp}/>
      </InputGroup>
      <Button variant='outline' block css={buttonStyle} intent='primary' iconBefore={<IconLogIn/>} loading={connecting} onClick={handleLoginButtonClick}>
        {TEXT.LOGIN.LOGIN_BUTTON}
      </Button>
      {
        props.admissionPeriod &&
        <Button block css={buttonStyle} variant='ghost' onClick={handleSignupButtonClick}>
          {TEXT.LOGIN.SIGNUP_BUTTON}
        </Button>
      }
    </React.Fragment>
    // </Layer>
  )

  if(isMobile)
    return(
      <div css={mainDivStyle}>
        {render()}
      </div>
    )
  else
    return(
      <Layer css={layerStyle}>
        {render()}
      </Layer>
    )

}

export default withRouter(Login)