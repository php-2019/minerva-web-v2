const PROTOCOL = 'http'
const SERVER_IP = 'localhost'
const PORT = 1038
const BASE_V1 = PROTOCOL + '://' + SERVER_IP + ':' + PORT + '/'

const API_ROUTES = {
  USERS: {
    LOGIN: BASE_V1 + 'login',
    STUDENTS: BASE_V1 + 'students',
    TEACHERS: BASE_V1 + 'teachers',
    ADMISSIONS: BASE_V1 + 'admissions',
    EXISTS: BASE_V1 + 'users/exists'
  },
  SUBJECTS: BASE_V1 + 'subjects',
  AREAS: BASE_V1 + 'areas',
  ADMINS: {
    TEACHERS: BASE_V1 + 'admins/teachers',
    AREAS: BASE_V1 + 'admins/areas',
    UNAPPROVED: BASE_V1 + 'admins/users/unapproved',
    APPROVE: BASE_V1 + 'admins/users/approve',
    CLASSROOMS: BASE_V1 + 'admins/classrooms',
    FREE_CLASSROOMS: BASE_V1 + 'admins/classrooms/free',
    ADMISSIONS: BASE_V1 + 'admins/admissions',
    SUBJECTS: BASE_V1 + 'admins/subjects',
    LESSONS: BASE_V1 + 'admins/lessons'
  }
}

export default API_ROUTES