const TEXT = {
  WEEK_DAYS: [
    'Domingo',
    'Lunes',
    'Martes',
    'Miércoles',
    'Jueves',
    'Viernes',
    'Sábado'
  ],
  MONTHS: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
  ],
  ALERTS: {
    GENERAL: {
      UNAUTHORIZED: {
        TITLE: 'No se encontró una sesión activa',
        SUBTITLE: 'Conéctate para acceder a la plataforma'
      },
      OUR_ERROR: {
        TITLE: 'Algo salió mal',
        SUBTITLE: '¡Lo sentimos! Por favor, prueba de nuevo más tarde'
      }
    },
    LOGIN_FAILED: {
      TITLE: 'Credenciales inválidas',
      SUBTITLE: 'Identificador o contraseña incorrectos'
    },
    SIGNUP: {
      USER_ALREADY_EXISTS: {
        TITLE: 'Usuario ya existe',
        SUBTITLE: 'Prueba con un nombre de usuario o un e-mail diferentes'
      },
      SIGNUP_FORM_ERROR: {
        TITLE: 'Formulario inválido',
        SUBTITLE: 'Revisa bien los campos, por favor'
      },
      SUCCESS: {
        TITLE: 'Registro exitoso',
        SUBTITLE: 'Recibirás un correo cuando tu cuenta haya sido aprobada, ¡buena suerte!'
      }
    },
    AREAS: {
      SUCCESS_CREATE: {
        TITLE: 'Área agregada',
        SUBTITLE: 'Nueva área creada con éxito'
      },
      SUCCESS_EDIT: {
        TITLE: 'Área editada',
        SUBTITLE: 'Área editada con éxito'
      }
    },
    ADMISSIONS: {
      SUCCESS_CHANGE_STATE: {
        TITLE: 'Listo',
        SUBTITLE: 'Acción realizada con éxito'
      }
    },
    SUBJECTS: {
      NAME_IN_USE: {
        TITLE: 'Nombre en uso',
        SUBTITLE: 'Ya existe una materia con ese nombre'
      },
      SUCCESS_CREATE: {
        TITLE: 'Materia creada',
        SUBTITLE: 'La materia se creó con éxito'
      },
      SUCCESS_EDIT: {
        TITLE: 'Materia editada',
        SUBTITLE: 'La materia se editó con éxito'
      }
    },
    CLASSROOMS: {
      SUCCESS_CREATE: {
        TITLE: 'Salón creado',
        SUBTITLE: 'El salón se creó con éxito'
      },
      SUCCESS_EDIT: {
        TITLE: 'Salón editado',
        SUBTITLE: 'El salón se editó con éxito'
      }
    },
    LESSONS: {
      SUCCESS_CREATE: {
        TITLE: 'Currícula creada',
        SUBTITLE: 'La currícula fue creada con éxito'
      }
    }
  },
  LOGIN: {
    FIELDS: {
      ID: {
        LABEL: 'Usuario, e-mail o documento',
        PLACEHOLDER: '@juan, juan@mail.com, 12345678...',
        ERROR: 'Debes ingresar un identificador'
      },
      PASSWORD: {
        LABEL: 'Contraseña',
        PLACEHOLDER: 'miContraseñaSuperSecreta1234',
        ERROR: 'Debes ingresar una contraseña'
      }
    },
    LOGIN_BUTTON: 'Iniciar sesión',
    SIGNUP_BUTTON: 'No tengo cuenta'
  },
  SIGNUP: {
    FIELDS: {
      USER_TYPE: {
        LABEL: 'Soy...',
        OPTIONS: {
          STUDENT: 'Estudiante',
          TEACHER: 'Docente'
        }
      },
      USERNAME: {
        LABEL: 'Nombre de usuario',
        PLACEHOLDER: 'juan23',
        ERRORS: {
          PRESENCE: 'Debes ingresar un nombre de usuario',
          FORMAT: 'El nombre de usuario debe tener entre 3 y 20 caracteres, y solo puede contener los símbolos: - . _',
          UNIQUENESS: 'Ese nombre de usuario ya está en uso'
        }
      },
      EMAIL: {
        LABEL: 'E-mail',
        PLACEHOLDER: 'juan@mail.com',
        ERRORS: {
          PRESENCE: 'Debes ingresar una dirección e-mail',
          FORMAT: 'La dirección e-mail ingresada no es válida'
        }
      },
      DOCUMENT: {
        LABEL: 'Cédula de identidad',
        PLACEHOLDER: '12345678',
        ERRORS: {
          PRESENCE: 'Debes ingresar un número de cédula, sin puntos ni guiones',
          FORMAT: 'El número de cédula ingresado no es válido'
        }
      },
      FIRST_NAME: {
        LABEL: 'Nombres',
        PLACEHOLDER: 'Juan Pedro',
        ERRORS: {
          PRESENCE: 'Debes ingresar tus nombres',
          FORMAT: 'Los nombres no deben superar los 255 caracteres en total'
        }
      },
      LAST_NAME: {
        LABEL: 'Apellidos',
        PLACEHOLDER: 'Pérez Rodríguez',
        ERRORS: {
          PRESENCE: 'Debes ingresar tus apellidos',
          FORMAT: 'Los apellidos no deben superar los 255 caracteres en total'
        }
      },
      PHONES: {
        LABEL: 'Teléfonos',
        PLACEHOLDER: '098765432',
        ADD_BUTTON: 'Agregar teléfono',
        ERRORS: {
          PRESENCE: 'Debes ingresar al menos un teléfono',
          FORMAT: 'Un teléfono solo puede contener números y el signo de + (solo uno al principio).'
        }
      },
      BIRTHDAY: {
        LABEL: 'Fecha de nacimiento',
        ERRORS: {
          PRESENCE: 'Debes ingresar tu fecha de nacimiento',
          FORMAT: 'Los apellidos no deben superar los 255 caracteres en total'
        }
      },
      SUBJECTS: {
        LABEL: 'Materias',
        ERRORS: {
          PRESENCE: 'Debes especificar las materias en las que te especializas'
        }
      },
      PASSWORD: {
        LABEL: 'Contraseña',
        PLACEHOLDER: 'miContraseñaSuperSecreta1234',
        ERRORS: {
          PRESENCE: 'Debes ingresar una contraseña',
          FORMAT: 'La contraseña debe tener mínimo 8 caracteres'
        }
      },
      PASSWORD_CONFIRMATION: {
        LABEL: 'Confirmar contraseña',
        PLACEHOLDER: 'miContraseñaSuperSecreta1234',
        ERRORS: {
          PRESENCE: 'Debes confirmar tu contraseña',
          FORMAT: 'Las contraseñas deben coincidir'
        }
      }
    },
    SIGNUP_BUTTON: 'Registrarse',
    LOGIN_ANCHOR: 'Ya tengo cuenta',
    AVAILABILITY_HELPER: {
      CHECKING: 'Comprobando disponibilidad...',
      SUCCESS: 'Disponible',
      FAIL: 'Ya está en uso'
    }
  },
  HOME: {
    TABS: {
      ADMIN: {
        ADMISSIONS: 'Altas',
        AREAS: 'Áreas',
        SUBJECTS: 'Materias',
        CLASSROOMS: 'Salones',
        CALENDAR: 'Currícula'
      },
      TEACHER: {
        ROLL: 'Lista',
        CLASSES: 'Clases'
      },
      STUDENT: {
        CLASES: 'Clases',
        SUBJECTS: 'Materias'
      },
      GENERAL: {
        PROFILE: {
          TITLE: 'Perfil',
          LOGOUT: 'Cerrar sesión'
        }
      }
    },
    VIEWS: {
      ADMIN: {
        AREAS: {
          TABLE: {
            HEADERS: {
              NAME: 'Nombre',
              TEACHER: 'Docente a cargo'
            },
            UNASSIGNED_TEACHER: 'No asignado',
            NO_AREAS_FOUND: 'No se encontraron áreas que mostrar'
          },
          ADD_BUTTON: 'Agregar área',
          DROPDOWN_MENU: {
            TITLE: 'Opciones',
            EDIT: 'Editar área',
            DELETE: 'Borrar área'
          },
          FORM: {
            ADD_BUTTON: 'Agregar área',
            EDIT_BUTTON: 'Editar área',
            CANCEL_BUTTON: 'Cancelar',
            FIELDS: {
              NAME: {
                LABEL: 'Nombre del área',
                PLACEHOLDER: 'Informática',
                ERRORS: {
                  PRESENCE: 'Debes especificar un nombre para la nueva área',
                  FORMAT: 'El nombre del área no debe superar los 255 caracteres',
                  UNIQUENESS: 'Ya existe un área con ese nombre'
                }
              },
              TEACHER: {
                LABEL: 'Docente a cargo',
                ERRORS: {
                  PRESENCE: 'Debes especificar un docente a cargo del área'
                }
              }
            }
          }
        },
        ADMISSIONS: {
          ADVANCED_OPTIONS: {
            BUTTON: 'Ver opciones avanzadas',
            ADMISSION_PERIOD_CHECKMARK: {
              LABEL: 'Período de inscripciones',
              ON: 'Habilitado',
              OFF: 'Deshabilitado'
            }
          },
          TABLE: {
            HEADERS: {
              NAME: 'Inscripción',
              TYPE: 'Tipo'
            },
            STUDENT_TYPE: 'Estudiante',
            TEACHER_TYPE: 'Docente',
            NO_ADMISSIONS_FOUND: 'No se encontraron inscripciones que aprobar'
          },
          INFO_DIALOG: {
            GHOST_BUTTON_LABEL: 'Ver más información',
            TITLE: (id) => (`Inscripción Nº${id + 1}`),
            TABLE: {
              HEADER: 'Información del inscripto',
              DOCUMENT: 'Documento',
              FULL_NAME: 'Nombre completo',
              USERNAME: 'Nombre de usuario',
              EMAIL: 'Correo electrónico',
              BIRTHDAY: 'Fecha de nacimiento',
              PHONES: 'Teléfonos',
              REGISTER_DATE: 'Fecha de inscripción'
            },
            APPROVE: 'Aprobar',
            REJECT: 'Rechazar'
          },
          BATCH_APPROVE: 'Aprobar seleccionados',
          BATCH_REJECT: 'Rechazar seleccionados'
        },
        SUBJECTS: {
          ADD_SUBJECT_BUTTON: 'Agregar materia',
          SUBJECT_IN_PROGRESS: 'Creando materia...',
          TABLE: {
            HEADERS: {
              AREA: 'Área',
              NAME: 'Nombre'
            },
            NO_SUBJECTS_FOUND: 'No se encontraron materias que mostrar'
          },
          DIALOG: {
            NEW_ROOT_TOPIC_BUTTON: 'Agregar tema a raíz',
            NO_TOPICS: 'Árbol de temas vacío',
            AREA_FIELD: {
              LABEL: 'Área',
              ERRORS: {
                PRESENCE: 'Debe elegir un área para la nueva materia'
              }
            },
            PRIORS_FIELD: {
              LABEL: 'Previaturas'
            },
            SUBJECT_NAME_FIELD: {
              LABEL: 'Nombre de materia',
              ERRORS: {
                PRESENCE: 'Debes darle un nombre a la materia',
                FORMAT: 'El nombre de la materia no debe superar los 255 caracteres'
              }
            },
            TOPIC_NAME_FIELD: {
              LABEL: 'Nombre del tema',
              PLACEHOLDER: 'Ej.: Trigonometría',
              ERRORS: {
                PRESENCE: 'Debes darle un nombre al nuevo tema',
                FORMAT: 'El nombre del tema no puede superar lo 255 caracteres'
              }
            },
            DROPDOWN_MENU: {
              ADD_TOPIC: 'Agregar subtema',
              EDIT_TOPIC: 'Editar tema',
              DELETE_TOPIC: 'Borrar tema'
            }
          }
        },
        CLASSROOMS: {
          ADD_BUTTON: 'Agregar salón',
          CREATING_CLASSROOM: 'Creando salón...',
          UNDEFINED_CAPACITY: '?',
          UNDEFINED_FLOOR: '?',
          DIALOG: {
            NAME_FIELD: {
              LABEL: 'Nombre del salón',
              ERRORS: {
                PRESENCE: 'Debes darle un nombre al salón',
                FORMAT: 'El nombre del salón no debe superar los 255 caracteres',
                UNIQUENESS: 'Ese nombre ya está en uso'
              }
            },
            CAPACITY_FIELD: {
              LABEL: 'Capacidad',
              ERRORS: {
                FORMAT: 'La capacidad de un salón debe ser mayor a cero'
              }
            },
            FLOOR_FIELD: {
              LABEL: 'Piso'
            }
          },
          TABLE: {
            HEADERS: {
              NAME: 'Nombre',
              CAPACITY: 'Capacidad',
              FLOOR: 'Piso'
            },
            NO_CLASSROOMS_FOUND: 'No se encontraron salones que mostrar'
          }
        },
        CALENDAR: {
          ADD_BUTTON: 'Agregar currícula',
          TABLE: {
            SUBJECT: 'Materia',
            NO_CURRICULAS_FOUND: 'No se encontraron currículas que mostrar'
          },
          WIDGET: {
            DAYS_INITIALS: [
              'LUN',
              'MAR',
              'MIE',
              'JUE',
              'VIE',
              'SÁB'
            ],
            REMOVE_BUTTON: 'Borrar',
            CONFIRM_BUTTON: 'Confirmar',
            CLEAN_BUTTON: 'Limpiar calendario',
            CREATE_BUTTON: 'Crear currícula',
            SHIFTS: function(shift) {
              switch(shift) {
                case 0:
                  return 'Turno matutino'
                case 1:
                  return 'Turno vespertino'
                case 2:
                  return 'Turno nocturno'
                default:
                  return null
              }
            },
            FIELDS: {
              SUBJECT: {
                LABEL: 'Materia'
              },
              TEACHER: {
                LABEL: 'Docente',
                ERRORS: {
                  PRESENCE: 'Debes elegir un docente que dicte la clase'
                }
              },
              CLASSROOM: {
                LABEL: 'Salón',
                ERRORS: {
                  PRESENCE: 'Debes elegir un salón donde dictar la clase'
                }
              }
            }
          }
        }
      }
    }
  }
}

export default TEXT