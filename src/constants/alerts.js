import TEXT from "./text-es"

const ALERTS = {
  GENERAL: {
    UNAUTHORIZED: {
      intent: 'danger',
      duration: 3000,
      title: TEXT.ALERTS.GENERAL.UNAUTHORIZED.TITLE,
      subtitle: TEXT.ALERTS.GENERAL.UNAUTHORIZED.SUBTITLE
    },
    OUR_ERROR: {
      intent: 'danger',
      duration: 3000,
      title: TEXT.ALERTS.GENERAL.OUR_ERROR.TITLE,
      subtitle: TEXT.ALERTS.GENERAL.OUR_ERROR.SUBTITLE
    }
  },
  LOGIN_FAILED: {
    intent: 'danger',
    duration: 3000,
    title: TEXT.ALERTS.LOGIN_FAILED.TITLE,
    subtitle: TEXT.ALERTS.LOGIN_FAILED.SUBTITLE
  },
  SIGNUP: {
    USER_ALREADY_EXISTS: {
      intent: 'danger',
      duration: 3000,
      title: TEXT.ALERTS.SIGNUP.USER_ALREADY_EXISTS.TITLE,
      subtitle: TEXT.ALERTS.SIGNUP.USER_ALREADY_EXISTS.SUBTITLE
    },
    SIGNUP_FORM_ERROR: {
      intent: 'danger',
      duration: 3000,
      title: TEXT.ALERTS.SIGNUP.SIGNUP_FORM_ERROR.TITLE,
      subtitle: TEXT.ALERTS.SIGNUP.SIGNUP_FORM_ERROR.SUBTITLE
    },
    SUCCESS: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.SIGNUP.SUCCESS.TITLE,
      subtitle: TEXT.ALERTS.SIGNUP.SUCCESS.SUBTITLE
    }
  },
  AREAS: {
    SUCCESS_CREATE: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.AREAS.SUCCESS_CREATE.TITLE,
      subtitle: TEXT.ALERTS.AREAS.SUCCESS_CREATE.SUBTITLE
    },
    SUCCESS_EDIT: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.AREAS.SUCCESS_EDIT.TITLE,
      subtitle: TEXT.ALERTS.AREAS.SUCCESS_EDIT.SUBTITLE
    }
  },
  ADMISSIONS: {
    SUCCESS_CHANGE_STATE: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.ADMISSIONS.SUCCESS_CHANGE_STATE.TITLE,
      subtitle: TEXT.ALERTS.ADMISSIONS.SUCCESS_CHANGE_STATE.SUBTITLE
    }
  },
  SUBJECTS: {
    NAME_IN_USE: {
      intent: 'danger',
      duration: 3000,
      title: TEXT.ALERTS.SUBJECTS.NAME_IN_USE.TITLE,
      subtitle: TEXT.ALERTS.SUBJECTS.NAME_IN_USE.SUBTITLE
    },
    SUCCESS_CREATE: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.SUBJECTS.SUCCESS_CREATE.TITLE,
      subtitle: TEXT.ALERTS.SUBJECTS.SUCCESS_CREATE.SUBTITLE
    },
    SUCCESS_EDIT: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.SUBJECTS.SUCCESS_EDIT.TITLE,
      subtitle: TEXT.ALERTS.SUBJECTS.SUCCESS_EDIT.SUBTITLE
    }
  },
  CLASSROOMS: {
    SUCCESS_CREATE: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.CLASSROOMS.SUCCESS_CREATE.TITLE,
      subtitle: TEXT.ALERTS.CLASSROOMS.SUCCESS_CREATE.SUBTITLE
    },
    SUCCESS_EDIT: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.CLASSROOMS.SUCCESS_EDIT.TITLE,
      subtitle: TEXT.ALERTS.CLASSROOMS.SUCCESS_EDIT.SUBTITLE
    }
  },
  LESSONS: {
    SUCCESS_CREATE: {
      intent: 'success',
      duration: 3000,
      title: TEXT.ALERTS.LESSONS.SUCCESS_CREATE.TITLE,
      subtitle: TEXT.ALERTS.LESSONS.SUCCESS_CREATE.SUBTITLE
    }
  }
}

export default ALERTS