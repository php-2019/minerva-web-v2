const USER_TYPES = {
  ADMIN: 0,
  TEACHER: 1,
  STUDENT: 2
}

export default USER_TYPES