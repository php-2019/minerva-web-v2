import TEXT from '../constants/text-es'

var GeneralHelpers = {}

GeneralHelpers.toISOString = (date) => {
  if(!date)
    return null;
  let year = date.getFullYear(),
      month = date.getMonth() + 1,
      day = date.getDate()
  return `${year}-${month > 9 ? month : '0' + month}-${day > 9 ? day : '0' + day}`
}

GeneralHelpers.isUserName = (username) => {
  let matches = username.match(/\W/g)
  if(matches)
    return username.length > 2 &&
      username.length < 256 &&
      !username.includes('@') &&
      !matches.find((match) => (match !== '-' && match !== '.'))
  else
    return username.length > 2 &&
      username.length < 256 &&
      !username.includes('@')
}

GeneralHelpers.isEmail = (email) => (/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/.test(email) && email.length < 256)

GeneralHelpers.isCedula = (doc) => (!(/\D/.test(doc)) && doc.length === 8)

GeneralHelpers.isPhone = (phone) => (
  phone.length > 5 &&
  (
    !(/\D/.test(phone)) ||
    (
      phone[0] === '+' &&
      !(/\D/.test(phone.slice(1)))
    )
  )
)

GeneralHelpers.dateToHumanReadableStr = (date) => {
  let weekDay = date.getDay(),
      monthDay = date.getDate(),
      month = date.getMonth(),
      year = date.getFullYear()
  return `${TEXT.WEEK_DAYS[weekDay]}, ${monthDay} de ${TEXT.MONTHS[month].toLowerCase()}, ${year}`
}

export default GeneralHelpers