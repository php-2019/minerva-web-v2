import ALERTS from "../constants/alerts";

var AuthHelpers = {}

AuthHelpers.getToken = (props, toast) => {
  let token = localStorage.getItem('minerva-token')
  if(typeof token === 'string')
    return 'Bearer ' + token
  else {
    console.error('Error while extracting token from localStorage. Got:', token)
    AuthHelpers.discardTokenAndRedirect(props, toast)
  }
}

AuthHelpers.discardTokenAndRedirect = (props, toast, loggingOut) => {
  localStorage.removeItem('minerva-token')
  localStorage.removeItem('minerva-usertype')
  localStorage.removeItem('minerva-userid')
  localStorage.removeItem('minerva-username')
  props.history.push('/enter')
  if(!loggingOut)
    toast(ALERTS.GENERAL.UNAUTHORIZED)
}

AuthHelpers.isEmail = (email) => (/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/.test(email))

export default AuthHelpers