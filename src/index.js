import React from 'react'
import ReactDOM from 'react-dom'
import {
  Route,
  BrowserRouter as Router,
  Switch
} from "react-router-dom"
import Header from './common/header'
import Dashboard from './views/dashboard'
import { Container, defaultTheme, ThemeProvider } from 'sancho';
import Home from './views/home'

const App = () => {

  var theme = {
    ...defaultTheme,
    fonts: {
      ...defaultTheme.fonts,
      base: 'Tiresias'
    }
  },
  mainContainerStyle = {
    display: 'block',
    margin: 'auto',
    // maxWidth: '840px'
    maxWidth: 'min-content'
  }

  return(
    <React.Fragment>
      <Header/>
      <ThemeProvider theme={theme}>
        <Container css={mainContainerStyle}>
          <Router>
            <Switch>
              <Route exact path='/' component={Dashboard}/>
              <Route path='/enter' component={Home}/>
            </Switch>
          </Router>
        </Container>
      </ThemeProvider>
    </React.Fragment>
  )
}

ReactDOM.render(<App/>, document.getElementById('root'))