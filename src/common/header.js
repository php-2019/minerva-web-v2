/** @jsx jsx */
import React from 'react'
import {Navbar, Toolbar} from 'sancho'
import {jsx} from '@emotion/core'

const Header = () => {
  var logoStyle = {
    width: '50px',
    height: '50px',
    paddingRight: '0.5rem'
  },
  mainDivStyle = {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    userSelect: 'none'
  },
  innerDivStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    fontFamily: 'Alegreya',
    fontSize: '2em',
    padding: '1rem',
    color: '#093d66'
  },
  separatorDivStyle = {
    marginBottom: '6rem'
  }

  return(
    <React.Fragment>
      <Navbar>
        <Toolbar>
          <div css={mainDivStyle}>
            <div css={innerDivStyle}>
              <img src="/assets/img/olive-wreath-blue.svg" alt="" css={logoStyle}/>
              Minerva
            </div>
          </div>
        </Toolbar>
      </Navbar>
      <div css={separatorDivStyle}></div>
    </React.Fragment>
  )
}

export default Header