/** @jsx jsx */
import React, { useState, useEffect } from 'react'
import { jsx, css } from '@emotion/core'
import TEXT from '../constants/text-es';
import { useTheme, Button, IconX, IconCheck, InputGroup, Select, Text, Divider, IconButton } from 'sancho';

const CurriculaWidget = (props) => {

  const [selectedDay, setSelectedDay] = useState(-1),
        [selectedPeriodStart, setSelectedPeriodStart] = useState(-1),
        [selectedPeriodEnd, setSelectedPeriodEnd] = useState(-1),
        [lessons, setLessons] = useState([]),
        [selectedTeacherIndexes, setSelectedTeacherIndexes] = useState([-1, -1, -1]),
        [selectedTeacherErrors, setSelectedTeacherErrors] = useState([null, null, null]),
        [selectedClassroomIndexes, setSelectedClassroomIndexes] = useState([-1, -1, -1]),
        [selectedClassroomErrors, setSelectedClassroomErrors] = useState([null, null, null]),
        [selectedSubjectIndex, setSelectedSubjectIndex] = useState(-1)

  const theme = useTheme(),
        dayNumbers = [1, 2, 3, 4, 5, 6],
        numberOfPeriods = 7

  const tHeaderStyle = css`
    padding: 0.75rem;
    user-select: none;
    -webkit-user-select: none;
  `,
  tdStyleDefault = css`
    text-align: center;
    padding: 0.75rem;
    user-select: none;
    -webkit-user-select: none;
  `

  useEffect(() => {
    if(selectedDay > -1 && selectedPeriodStart > -1)
      props.getFreeClassrooms(selectedDay, selectedPeriodStart, selectedPeriodEnd, props.subjects[selectedSubjectIndex].id)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedPeriodEnd])

  const isCellSelected = (day, period) => (selectedDay === day && period >= selectedPeriodStart && period <= selectedPeriodEnd)
  const isCellConfirmed = (day, period) => (lessons.some((obj) => (obj.day === day && period >= obj.periods[0] && period <= obj.periods[1])))

  const handleCellClick = (ev, day, period) => {
    if(!lessons.some((obj) => (obj.day === day))) {
      if(selectedDay > -1 && selectedPeriodStart > -1 && selectedPeriodEnd > -1) {
        setSelectedDay(-1)
        setSelectedPeriodStart(-1)
        setSelectedPeriodEnd(-1)
      } else {
        if(selectedDay > -1 && selectedPeriodStart > -1 && selectedDay !== day) {
          setSelectedDay(-1)
          setSelectedPeriodStart(-1)
          setSelectedPeriodEnd(-1)
        } else {
          setSelectedDay(day)
          if(selectedPeriodStart > -1) {
            if(period >= selectedPeriodStart)
              setSelectedPeriodEnd(period)
            else {
              setSelectedPeriodEnd(selectedPeriodStart)
              setSelectedPeriodStart(period)
            }
          } else
            setSelectedPeriodStart(period)
        }
      }
    }
  }

  const handleCellMouseEnter = (ev, day, period) => {
    let target = ev.currentTarget
    if((day === selectedDay && period === selectedPeriodStart) || isCellSelected(day, period))
      target.style.backgroundColor = theme.colors.intent.primary.light
    else if(isCellConfirmed(day, period))
      target.style.backgroundColor = theme.colors.intent.success.light
    else
      target.style.backgroundColor = theme.colors.background.tint1
  }

  const handleCellMouseLeave = (ev, day, period) => {
    let target = ev.currentTarget
    if((day === selectedDay && period === selectedPeriodStart) || isCellSelected(day, period))
      target.style.backgroundColor = theme.colors.intent.primary.lightest
    else if(isCellConfirmed(day, period))
      target.style.backgroundColor = theme.colors.intent.success.lightest
    else
      target.style.backgroundColor = theme.colors.background.default
  }

  const cleanCalendar = () => {
    setLessons([])
    setSelectedDay(-1)
    setSelectedPeriodStart(-1)
    setSelectedPeriodEnd(-1)
  }

  const removeSelectedPeriod = () => {
    setSelectedDay(-1)
    setSelectedPeriodStart(-1)
    setSelectedPeriodEnd(-1)
  }

  const resetLessonForm = () => {
    setSelectedTeacherIndexes([-1, -1, -1])
    setSelectedClassroomIndexes([-1, -1, -1])
    setSelectedTeacherErrors([null, null, null])
    setSelectedClassroomErrors([null, null, null])
  }

  const confirmPeriod = () => {
    if(!(selectedTeacherIndexes.some((index) => (index === -1)) && selectedClassroomIndexes.some((index) => (index === -1)))) {
      let newLessons = [...lessons]
      newLessons.push({
        day: selectedDay,
        periods: [selectedPeriodStart, selectedPeriodEnd],
        id_teacher: selectedTeacherIndexes.map((index) => (props.teachers[index].id)),
        id_classroom: selectedClassroomIndexes.map((index) => (props.freeClassrooms[index])),
      })
      setLessons(newLessons)
      removeSelectedPeriod()
      resetLessonForm()
    } else {
      let newTeacherErrors = [...selectedTeacherErrors],
          newClassroomErrors = [...selectedClassroomErrors]
      for(let i = 0; i < 3; i++) {
        if(selectedTeacherIndexes[i] < 0)
          newTeacherErrors[i] = TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.TEACHER.ERRORS.PRESENCE
        if(selectedClassroomIndexes[i] < 0)
          newClassroomErrors[i] = TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.CLASSROOM.ERRORS.PRESENCE
      }
      setSelectedTeacherErrors(newTeacherErrors)
      setSelectedClassroomErrors(newClassroomErrors)
    }
  }

  const handleTeacherSelectChange = (ev, shift) => {
    let newIndexes = [...selectedTeacherIndexes],
        newTeacherErrors = [...selectedTeacherErrors]
    newIndexes[shift] = Number(ev.currentTarget.value)
    newTeacherErrors[shift] = null
    setSelectedTeacherIndexes(newIndexes)
    setSelectedTeacherErrors(newTeacherErrors)
  }

  const handleClassroomSelectChange = (ev, shift) => {
    let newIndexes = [...selectedClassroomIndexes],
        newClassroomErrors = [...selectedClassroomErrors]
    newIndexes[shift] = Number(ev.currentTarget.value)
    newClassroomErrors[shift] = null
    setSelectedClassroomIndexes(newIndexes)
    setSelectedClassroomErrors(newClassroomErrors)
  }

  const handleSubjectChange = (ev) => {
    let newIndex = Number(ev.currentTarget.value)
    setSelectedSubjectIndex(newIndex)
  }

  const closeModal = () => {
    props.onClose()
  }

  const getTeacherOptions = () => {
    return props.teachers.map((teacher, index) => {
      return(
        <option key={index} value={index}>{teacher.fullname}</option>
      )
    })
  }

  const getClassroomOptions = () => {
    return props.freeClassrooms.map((classroom, index) => {
      let classroomObj = props.allClassrooms.find((c) => (c.id === classroom))
      return(
        <option key={index} value={index}>{classroomObj.name}</option>
      )
    })
  }

  const getSubjectOptions = () => {
    return props.subjects.map((subject, index) => {
      return(
        <option key={index} value={index}>{subject.name}</option>
      )
    })
  }

  const getSelects = () => {
    return [0, 1, 2].map((shift) => {
      return(
        <React.Fragment key={shift}>
          <Divider/>
          <Text variant='lead'>{TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.SHIFTS(shift)}</Text>
          <InputGroup
            label={TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.TEACHER.LABEL}
            error={selectedTeacherErrors[shift]}
          >
            <Select value={selectedTeacherIndexes[shift]} onChange={(ev) => handleTeacherSelectChange(ev, shift)}>
              <option value={-1}>{TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.TEACHER.LABEL}</option>
              {getTeacherOptions()}
            </Select>
          </InputGroup>
          <InputGroup
            label={TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.CLASSROOM.LABEL}
            error={selectedClassroomErrors[shift]}
          >
            <Select value={selectedClassroomIndexes[shift]} onChange={(ev) => handleClassroomSelectChange(ev, shift)}>
              <option value={-1}>{TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.CLASSROOM.LABEL}</option>
              {getClassroomOptions()}
            </Select>
          </InputGroup>
        </React.Fragment>
      )
    })
  }

  const getTableHeaders = () => {
    return TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.DAYS_INITIALS
      .map((dayName, index) => (<th key={index} css={tHeaderStyle}>{dayName}</th>))
  }

  const getTableCells = (period) => {
    return dayNumbers.map((day, index) => {
      return(
        <td
          key={index}
          css={tdStyleDefault}
          style={{
            backgroundColor:
              ((day === selectedDay && period === selectedPeriodStart)
              || isCellSelected(day, period))
              ? theme.colors.intent.primary.lightest
              : (isCellConfirmed(day, period)
              ? theme.colors.intent.success.lightest
              : theme.colors.background.default)
          }}
          onClick={(ev) => handleCellClick(ev, day, period)}
          onMouseEnter={(ev) => handleCellMouseEnter(ev, day, period)}
          onMouseLeave={(ev) => handleCellMouseLeave(ev, day, period)}
        >
          {day + period}
        </td>
      )
    })
  }

  const getTableRows = () => {
    let rowsArr = []
    for(let i = 0; i < numberOfPeriods; i++) {
      rowsArr.push(
        <tr key={i}>
          {getTableCells(i)}
        </tr>
      )
    }
    return rowsArr
  }

  return(
    <div
      css={
        css`
          display: flex;
          flex-direction: column;
          align-items: center;
          overflow-y: auto;
          max-height: 90vh;
        `
      }
    >
      <div css={css`display: flex; justify-content: flex-end; width: 100%;`}>
        <IconButton
          label=''
          variant='ghost'
          icon={<IconX/>}
          onClick={closeModal}
        />
      </div>
      {
        props.addingCurricula &&
        <div css={css`width: 100%; margin-bottom: 1rem;`}>
          <InputGroup
            label={TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.SUBJECT.LABEL}
            css={css`width: 98%;`}
          >
            <Select value={selectedSubjectIndex} onChange={handleSubjectChange} disabled={selectedSubjectIndex > -1}>
              <option value={-1}>{TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.FIELDS.SUBJECT.LABEL}</option>
              {getSubjectOptions()}
            </Select>
          </InputGroup>
        </div>
      }
      <Divider/>
      {
        (!props.addingCurricula || selectedSubjectIndex > -1) &&
        <table css={css`color: ${theme.colors.text.muted}; border-collapse: collapse;`}>
          <thead>
            <tr>
              {getTableHeaders()}
            </tr>
          </thead>
          <tbody>
            {getTableRows()}
          </tbody>
        </table>
      }
      {
        props.addingCurricula && lessons.length > 0 &&
        <Button
          variant='outline'
          intent='primary'
          block
          css={css`${selectedDay > -1 && selectedPeriodStart > -1 && selectedPeriodEnd > -1 ? 'margin-top: 1rem; min-height: 40px;' : ''}`}
          iconBefore={<IconX/>}
          onClick={cleanCalendar}
        >
          {TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.CLEAN_BUTTON}
        </Button>
      }
      {
        props.addingCurricula && selectedDay > -1 && selectedPeriodStart > -1 && selectedPeriodEnd > -1 &&
        <React.Fragment>
          <div css={css`width: 100%; margin-top: 1rem;`}>
            {getSelects()}
          </div>
          <Divider/>
          <div css={css`width: 100%; margin-top: 1rem;`}>
            <div css={css`display: flex; justify-content: space-between;`}>
              <Button
                variant='outline'
                intent='danger'
                css={css`width: 49%;`}
                iconBefore={<IconX/>}
                onClick={removeSelectedPeriod}
              >
                {TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.REMOVE_BUTTON}
              </Button>
              <Button
                variant='outline'
                intent='success'
                css={css`width: 49%;`}
                iconBefore={<IconCheck/>}
                onClick={confirmPeriod}
              >
                {TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.CONFIRM_BUTTON}
              </Button>
            </div>
          </div>
        </React.Fragment>
      }
      {
        props.addingCurricula && lessons.length > 0 &&
        <Button
          variant='outline'
          intent='success'
          iconBefore={<IconCheck/>}
          css={css`width: 100%; margin-top: 1rem;`}
          onClick={() => props.onCreate(lessons, props.subjects[selectedSubjectIndex].id)}
        >
          {TEXT.HOME.VIEWS.ADMIN.CALENDAR.WIDGET.CREATE_BUTTON}
        </Button>
      }
    </div>
  )

}

export default CurriculaWidget