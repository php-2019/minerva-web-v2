require 'sinatra'
require 'json'
require 'sinatra/cross_origin'

register Sinatra::CrossOrigin

configure do
  enable :cross_origin
end

# set :allow_methods, [:get, :post, :patch, :delete, :options, :head]

before do
  content_type 'application/json'
end

options "*" do
  response.headers["Allow"] = "HEAD,GET,PUT,POST,PATCH,DELETE,OPTIONS"
  response.headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept, Authorization"
  204
end

# Main index

get '/something' do
  status 401 unless request.env['HTTP_AUTHORIZATION'].size > 0
end

post '/login' do
  {
    token: 'MANZANAAA',
    userType: 0
  }.to_json
end

get '/signup' do

end